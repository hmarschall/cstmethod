/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2011 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    clusteredDecomp

SourceFiles
    clusteredDecomp.C

Authors
   Christian Kunkelmann (2011), Stefan Batzdorf (2015), Institute of 
   Thermodynamics, TU Darmstadt

\*---------------------------------------------------------------------------*/

#include "clusteredDecomp.H"
#include "addToRunTimeSelectionTable.H"
#include "HashTable.H"
#include "dynamicRefineFvMesh.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{
    defineTypeNameAndDebug(clusteredDecomp, 0);

    addToRunTimeSelectionTable
    (
        decompositionMethod,
        clusteredDecomp,
        dictionary
    );
}


// * * * * * * * * * * * * * Private Member Functions  * * * * * * * * * * * //




// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::clusteredDecomp::clusteredDecomp(const dictionary& decompositionDict)
:
    decompositionMethod(decompositionDict),
    clusterDict_(decompositionDict_.subDict(typeName + "Coeffs"))
{
    const word subMethodType(clusterDict_.lookup("method"));

    if ((subMethodType != "simple") && (subMethodType != "hierarchical"))
    {
        FatalErrorIn("clusteredDecomp::clusteredDecomp(const dictionary&)")
            << "Invalid decomposition method " << subMethodType 
            << " for decomposition of clusters." << endl << endl
            << "Valid decompositionMethods are :" << endl
            << "2" << endl << "(" << endl << "simple" << endl
            << "hierarchical" << endl << ")" << endl
            << exit(FatalError);
    }

    label nClusterSubDomains = readLabel(clusterDict_.lookup("numberOfSubdomains"));
    if (nDomains() != nClusterSubDomains)
    {
        FatalErrorIn("clusteredDecomp::clusteredDecomp(const dictionary&)")
            << "Top level decomposition specifies " << nDomains()
            << " domains which is not equal to the number of subdomains of "
            << nClusterSubDomains << " specified in the decomposition method "
            << "chosen for the clusters"
            << exit(FatalError);

    }

    Info << "For clustered decomposition ";
    subMethod_ = decompositionMethod::New(clusterDict_);

    subMethodDict_ = clusterDict_.subDict(subMethodType + "Coeffs");
}


// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

bool Foam::clusteredDecomp::parallelAware() const
{
    return subMethod_().parallelAware();
}


Foam::labelList Foam::clusteredDecomp::decompose
(
    const pointField& points,
    const scalarField& weights,
    const refinementHistory& history
)
{
    labelField finalDecomp(points.size(), -1);
    List<label> cellLevel (points.size(), -1);

    Info << "Determining clusters" << endl;

    List<DynamicList<label> > daughters (history.splitCells().size(), DynamicList<label>(0));
    label nParents = 0;
    label nUnrefined = 0;

    forAll(points, iCell)
    {
        label index = history.visibleCells()[iCell];

        if (index >= 0)
        {
            label highestParent = history.splitCells()[index].parent_;

            //- check if splitCell really has a parent, history might not be compact!
            if (highestParent >= 0)
            {
                label level = 1;
                while (history.splitCells()[highestParent].parent_ >= 0)
                {
                    highestParent = history.splitCells()[highestParent].parent_;
                    level++;
                }

                if (daughters[highestParent].size() == 0)
                {
                    // found a new parent
                    nParents++;
                }

                daughters[highestParent].append(iCell);
                cellLevel[iCell] = level;
            }
            else
            {
                cellLevel[iCell] = 0;
                nUnrefined++;
            }
        }
        else
        {
            cellLevel[iCell] = 0;
            nUnrefined++;
        }
    }

    label nClusters = nParents+nUnrefined;
    pointField clusterCentres (nClusters, vector(0,0,0));
    scalarField clusterWeights (nClusters, 0.0);

    //- order: first all the clusters, then all the unrefinedCells
    label iCluster = 0;
    forAll(daughters, iParent)
    {
        const DynamicList<label>& thisDaughters = daughters[iParent];
        if (thisDaughters.size() > 0)
        {
            vector centre (0,0,0);
            scalar sumWeights = 0.0;
            forAll(thisDaughters, iDaughter)
            {
                scalar level = double(cellLevel[thisDaughters[iDaughter]]);
                scalar centreWeight = 1.0/Foam::pow(8.0,level);
                centre += points[thisDaughters[iDaughter]]*centreWeight;
                sumWeights += centreWeight;
                clusterWeights[iCluster] += weights[thisDaughters[iDaughter]];
            }
            centre /= sumWeights;
            clusterCentres[iCluster] = centre;
            iCluster++;
        }
    }

    forAll(cellLevel, iCell)
    {
        if (cellLevel[iCell] == 0)
        {
            clusterCentres[iCluster] = points[iCell];
            clusterWeights[iCluster] = weights[iCell];
            iCluster++;
        }
    }


    //- distribute clusters sub-decomposition method
    Info << "Distributing clusters to processors" << endl;

    labelList clusterDecomp = subMethod_().decompose(clusterCentres, clusterWeights);


    //- transfer cluster decomposition to cell finalDecomp
    iCluster = 0;
    forAll(daughters, iParent)
    {
        const DynamicList<label>& thisDaughters = daughters[iParent];
        if (thisDaughters.size() > 0)
        {
            forAll(thisDaughters, iDaughter)
            {
                finalDecomp[thisDaughters[iDaughter]] = clusterDecomp[iCluster];
            }
            iCluster++;
        }
    }

    forAll(cellLevel, iCell)
    {
        if (cellLevel[iCell] == 0)
        {
            finalDecomp[iCell] = clusterDecomp[iCluster];
            iCluster++;
        }
    }

    return finalDecomp;
}

Foam::labelList Foam::clusteredDecomp::decompose
(
    const polyMesh& mesh,
    const pointField& points,
    const scalarField& weights
)
{
    //- first try to case mesh into dynamicRefineFvMesh
    if (isA<dynamicRefineFvMesh>(mesh))
    {
        const dynamicRefineFvMesh& refineMesh = refCast<const dynamicRefineFvMesh>(mesh);
        Info << "Found dynamicRefineFvMesh in region: " << mesh.name() << endl;
        return decompose(points, weights, refineMesh.meshCutter().history());
    }

    //- else try to read history from db 
    if (mesh.db().found("refinementHistory"))
    {
        HashTable<const refinementHistory*> histories = mesh.db().subRegistry(mesh.name()).lookupClass<refinementHistory>();

        if (histories.size() != 1)
        {
            FatalErrorIn("clusteredDecomp::decompose(const polyMesh&, const pointField&, const scalarField&)")
                << "Found multiple object of Type refinementHistory in objectRegistry "
                << "of " << mesh.name() << endl
                << "Don't know what to do!"
                << exit(FatalError);
        }

        for (HashTable<const refinementHistory*>::const_iterator iter = histories.begin(); iter != histories.end(); ++iter)
        {
            const refinementHistory* history_ = dynamic_cast<const refinementHistory*>(iter());
            Info << "Found refinementHistory in object Registry of region: " << mesh.name() << endl;
            return decompose(points, weights, *history_);
        }
    }

    //- Try to read history from file (e.g. when called from decomposePar)
    IOobject historyHeader
    (
        "refinementHistory",
        mesh.pointsInstance(),
        polyMesh::meshSubDir,
        mesh,
        IOobject::MUST_READ,
        IOobject::NO_WRITE,
        false
    );

    if (historyHeader.headerOk())
    {
        Info << "Reading refinementHistory from " << mesh.pointsInstance() << "/" << polyMesh::meshSubDir << endl;
        refinementHistory history (historyHeader);
        return decompose(points, weights, history);
    }

    //- Otherwise no history is present
    Info << "No refinementHistory found, doing nothing";
    return subMethod_().decompose(mesh,points,weights);
}


// ************************************************************************* //

#!/bin/bash

cd ${0%/*} || exit 1    # run from this directory

. etc/bashrc            # source the bashrc to set additional env-variables

# clean Interface Capturing Schemes - Library
cd $CST_PROJECT/src/interfaceCapturingSchemes
wclean all
rm $FOAM_USER_LIBBIN/libInterfaceCapturingSchemes.so

# clean additional HR schemes
cd $CST_PROJECT/src/HiresSchemes
wclean all
rm $FOAM_USER_LIBBIN/libHiresSchemes.so

# clean reconstruction and curvature models
cd $CST_PROJECT/src/transportModels/interface
wclean all
rm $FOAM_USER_LIBBIN/libinterface.so

# clean twoPhaseMixture
cd $CST_PROJECT/src/transportModels/twoPhaseMixture
wclean all
rm $FOAM_USER_LIBBIN/libTwoPhaseMixture.so

# clean finiteVolume class additions
cd $CST_PROJECT/src/finiteVolume
wclean all
rm $FOAM_USER_LIBBIN/libfiniteVolumeUsr.so

# clean speciesTransferModel (CST model library)
cd $CST_PROJECT/src/transportModels/speciesTransferModel
wclean all
rm $FOAM_USER_LIBBIN/libcstModels.so

# clean reactiveMixture (chemical reaction library)
cd $CST_PROJECT/src/transportModels/reactiveMixture
wclean all
rm $FOAM_USER_LIBBIN/libreactiveMixture.so

# clean loadBalancing library
cd $CST_PROJECT/loadBalancing/src
wclean dynamicMesh
rm $FOAM_USER_LIBBIN/libdynamicMesh.so
wclean dynamicFvMesh
rm $FOAM_USER_LIBBIN/libdynamicFvMesh.so
wclean dynamicRefineBalancedFvMesh
rm $FOAM_USER_LIBBIN/libdynamicFvMesh-dev.so
wclean userDecompositionMethods
rm $FOAM_USER_LIBBIN/libuserDecompositionMethods.so

cd $CST_PROJECT/loadBalancing/utilities
wclean decomposeParLevel
rm $FOAM_USER_APPBIN/decomposeParLevel
wclean reconstructParLevel
rm $FOAM_USER_APPBIN/reconstructParLevel
wclean execRefinement
rm $FOAM_USER_APPBIN/execRefinement
wclean testUtil
rm $FOAM_USER_APPBIN/testUtil

# clean solvers
cd $CST_PROJECT/applications/solvers/interCSTFoam
./Allwclean

# clean pre processing tools
cd $CST_PROJECT/applications/utilities/preProcessing
wclean initField
rm $FOAM_USER_APPBIN/initField
wclean initConcentration
rm $FOAM_USER_APPBIN/initConcentration

# clean post processing tools
cd $CST_PROJECT/applications/utilities/postProcessing
wclean createBubbleHis
rm $FOAM_USER_APPBIN/createBubbleHis
wclean calculateAverageBubbleSpeciesConcentration
rm $FOAM_USER_APPBIN/averageSpeciesConcentration
wclean calcGlobalSherwoodFromAverageConcentration
rm $FOAM_USER_APPBIN/calcGlobalSherwoodFromAverageConcentration

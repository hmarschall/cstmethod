/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 1991-2008 OpenCFD Ltd.
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

Application
    initField

SourceFiles
    initField.C

Authors
    Daniel Deising     <deising@mma.tu-darmstadt.de>
    Daniel Rettenmaier <rettenmaier@gsc.tu-darmstadt.de>
    All rights reserved.

Description
    Field initialisation tool to create sphere, box, zalesak disc

    You may refer to this software as :
    //- full bibliographic data to be provided

    This code has been developed by :
        Daniel Deising (main developer).
    
    Method Development and Intellectual Property :
        Daniel Deising       <deising@mma.tu-darmstadt.de>
        Daniel Rettenmaier   <rettenmaier@gsc.tu-darmstadt.de>
        Holger Marschall     <marschall@mma.tu-darmstadt.de>
        Dieter Bothe         <bothe@mma.tu-darmstadt.de>
        Cameron Tropea       <ctropea@sla.tu-darmstadt.de>

        Mathematical Modeling and Analysis
        Institute for Fluid Mechanics and Aerodynamics
        Center of Smart Interfaces
        Technische Universitaet Darmstadt
       
    If you use this software for your scientific work or your publications,
    please don't forget to acknowledge explicitly the use of it.

\*---------------------------------------------------------------------------*/

#include "fvCFD.H"
#include "dynamicFvMesh.H"
#include <iostream>
#include <fstream>

using namespace Foam;

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

int main(int argc, char *argv[])
{

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

    #include "options.H"
    #include "createDynamicFvMesh.H"
    

    volScalarField alpha1
    (
        IOobject
        (
            field,
            time,
            mesh,
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
        mesh,
        dimensionedScalar(field, dimless, 0.),
        "zeroGradient"
    );
    
    volScalarField alphaS
    (
        IOobject
        (
            "alphaS",
            runTime.timeName(),
            mesh,
            IOobject::READ_IF_PRESENT,
            IOobject::NO_WRITE
        ),
        mesh,
        dimensionedScalar("alphaS", dimless, 0.0),
        zeroGradientFvPatchScalarField::typeName
    );


    {
        Info << "set field at Time: "<< runTime.timeName() << endl;

        //- based on faveAverage in funkySetFields: 
        //  initialize surface Field and then use fvc::average functionality
        surfaceScalarField alphaf(linearInterpolate(alpha1));

        // init sphere
        if (shape == "sphere")
        {
            forAll(alphaf, faceI)
            {
                scalar x = mesh.Cf()[faceI].component(0);
                scalar y = mesh.Cf()[faceI].component(1);
                scalar z = mesh.Cf()[faceI].component(2);
                scalar r = Foam::sqrt(pow(x-x0,2)+pow(y-y0,2)+pow(z-z0,2));
            
                alphaf[faceI] = pos(R - r);
            }
            
            forAll(alphaf.boundaryField(), iPatch)
            {
                forAll(alphaf.boundaryField()[iPatch], iFace)
                {
                    scalar x = mesh.boundaryMesh()[iPatch].faceCentres()[iFace].component(0);
                    scalar y = mesh.boundaryMesh()[iPatch].faceCentres()[iFace].component(1);
                    scalar z = mesh.boundaryMesh()[iPatch].faceCentres()[iFace].component(2);
                    scalar r = Foam::sqrt(pow(x-x0,2)+pow(y-y0,2)+pow(z-z0,2));
        
                    alphaf.boundaryFieldRef()[iPatch][iFace] = pos(R - r);
                }
            }
        }
        else if (shape == "zalesak")
        {
            forAll(alphaf, faceI)
            {
                scalar x = mesh.Cf()[faceI].component(0);
                scalar y = mesh.Cf()[faceI].component(1);
                scalar z = mesh.Cf()[faceI].component(2);
                scalar r = Foam::sqrt(pow(x-x0,2)+pow(y-y0,2)+pow(z-z0,2));
            
                alphaf[faceI] = pos(R - r);

                // introduce cut (width x depth)
                if (mag(x-x0) < width/2 && (y-y0) >= (R - depth))
                {
                    alphaf[faceI] = 0;
                }
            }
            
            forAll(alphaf.boundaryField(), iPatch)
            {
                forAll(alphaf.boundaryField()[iPatch], iFace)
                {
                    scalar x = mesh.boundaryMesh()[iPatch].faceCentres()[iFace].component(0);
                    scalar y = mesh.boundaryMesh()[iPatch].faceCentres()[iFace].component(1);
                    scalar z = mesh.boundaryMesh()[iPatch].faceCentres()[iFace].component(2);
                    scalar r = Foam::sqrt(pow(x-x0,2)+pow(y-y0,2)+pow(z-z0,2));
        
                    alphaf.boundaryFieldRef()[iPatch][iFace] = pos(R - r);
                    
                    // introduce cut (width x depth)
                    if (mag(x-x0) < width/2 && (y-y0) >= (R - depth))
                    {
                        alphaf.boundaryFieldRef()[iPatch][iFace] = 0;
                    }
                }
            }
        }
        else if (shape == "box")
        {
            scalar xmax = x0 + width/2;
            scalar xmin = x0 - width/2;

            scalar ymax = y0 + depth/2;
            scalar ymin = y0 - depth/2;

            scalar zmax = z0 + length/2;
            scalar zmin = z0 - length/2;

            forAll(alphaf, faceI)
            {
                scalar x = mesh.Cf()[faceI].component(0);
                scalar y = mesh.Cf()[faceI].component(1);
                scalar z = mesh.Cf()[faceI].component(2);

                alphaf[faceI] = neg((x-xmax)*(x-xmin))*neg((y-ymax)*(y-ymin))*neg((z-zmax)*(z-zmin));
            }
            
            forAll(alphaf.boundaryField(), iPatch)
            {
                forAll(alphaf.boundaryField()[iPatch], iFace)
                {
                    scalar x = mesh.boundaryMesh()[iPatch].faceCentres()[iFace].component(0);
                    scalar y = mesh.boundaryMesh()[iPatch].faceCentres()[iFace].component(1);
                    scalar z = mesh.boundaryMesh()[iPatch].faceCentres()[iFace].component(2);
        
                    alphaf.boundaryFieldRef()[iPatch][iFace] = neg((x-xmax)*(x-xmin))*neg((y-ymax)*(y-ymin))*neg((z-zmax)*(z-zmin));
                }
            }
        }
        else
        {
            FatalErrorIn
            (
                "initField"
            )   << "Unknown field shape "
                << shape << nl << nl
                << "Valid shapes are : " << endl
                << "sphere, box, zalesak, ellipsoid"
                << exit(FatalError);
        }

        alpha1=fvc::average(alphaf);

        //- create a smooth version of the alpha field
        if(isSmooth)
        {
            dimensionedScalar DAlpha = min(pow(smoothFactor/alpha1.mesh().deltaCoeffs(),2));
    
            fvScalarMatrix alphaSEqn
            (
                fvm::Sp(scalar(1),alphaS) - fvm::laplacian(DAlpha,alphaS) == alpha1
            );
            
            alphaSEqn.solve();
            alphaS.correctBoundaryConditions();
        }

        alpha1.correctBoundaryConditions();
    }

    runTime.writeNow();
    if (isSmooth)
    {
        alphaS.write();
    }

    Info<< "End\n" << endl;

    return(0);
}


// ************************************************************************* //

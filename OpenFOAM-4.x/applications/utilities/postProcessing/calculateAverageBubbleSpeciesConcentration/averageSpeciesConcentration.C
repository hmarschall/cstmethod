/*---------------------------------------------------------------------------* \
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2011 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Application
    averageSpeciesConcentration

Authors
    Daniel Deising     <deising@mma.tu-darmstadt.de>
    All rights reserved.

Description
    Calculates the average species concentration in phase alpha1.
    The -noWrite option just outputs the max/min values without writing
    the field.

    You may refer to this software as :
    //- Deising et. al. 2016, Chem. Eng. Sci. 139, pp. 173-195

    This code has been developed by :
        Daniel Deising (main developer).
    
    Method Development and Intellectual Property :
        Daniel Deising   <deising@mma.tu-darmstadt.de>
        Holger Marschall <marschall@mma.tu-darmstadt.de>
        Dieter Bothe     <bothe@mma.tu-darmstadt.de>

        Mathematical Modeling and Analysis
        Center of Smart Interfaces
        Technische Universitaet Darmstadt
       
    If you use this software for your scientific work or your publications,
    please don't forget to acknowledge explicitly the use of it.

\*---------------------------------------------------------------------------*/

#include "fvc.H"
#include "OFstream.H"
#include "fvCFD.H"
#include "argList.H"
#include "IOobjectList.H"
#include "dynamicFvMesh.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

int main(int argc, char *argv[])
{
    // add arguments to function call:
    Foam::timeSelector::addOptions();
#   include "addRegionOption.H"
    argList::validOptions.insert("write", "");
    argList::validOptions.insert("mrf", "");

    // create list of time dirs
#   include "setRootCase.H"
#   include "createTime.H"

    instantList timeDirs = timeSelector::select0(runTime, args);
//#   include "createNamedMesh.H"

    #include "createDynamicFvMesh.H"
    
    // read updated mesh
    mesh.readUpdate();

    // read arguments if present
    bool writeResults = args.optionFound("write");
    bool isMRF = args.optionFound("mrf");

    // create/open file averageSpeciesConcentration.dat
    Info << "wird ausgeführt " << endl;
    ofstream fout("averageSpeciesConcentration.dat", ios_base::app);
    if (runTime.value() == 0)
    {
        fout<< "\t time \t average bubble velocity (y) \t bubble surface area \t average species concentration (C0,C1,...) \n" << endl;
    }

    // loop over all time dirs found:
    forAll(timeDirs, timeI)
    {
        runTime.setTime(timeDirs[timeI], timeI);
        Info<< "Time = " << runTime.timeName() << endl;
        
        mesh.readUpdate();

        IOobject Uheader
        (
            "U",
            runTime.timeName(),
            mesh,
            IOobject::MUST_READ
        );
 
        IOobject alphaHeader
        (
            "alpha1",
            runTime.timeName(),
            mesh,
            IOobject::MUST_READ
        );

        IOdictionary speciesProperties
        (
            IOobject
            (
                "speciesProperties",
                runTime.constant(),
                runTime,
                IOobject::MUST_READ,
                IOobject::NO_WRITE
            )
        );

        // Determine number of species concentration fields
        scalar n=0;

        // Search for list of objects at startTime
        IOobjectList objects(mesh,runTime.timeName());

        // Search list of objects for volScalarFields
        IOobjectList scalarFields
        (
            objects.lookupClass("volScalarField")
        );
 
        for
        (
            IOobjectList::iterator scalarFieldIter 
            = scalarFields.begin();
            scalarFieldIter != scalarFields.end();
            ++ scalarFieldIter
        )
        {
        // Read field
            volScalarField field
            (
                *scalarFieldIter(),
                 mesh
            );
            word fieldname = field.name();
            
            if (fieldname.find("C")==0 && fieldname[2]!='_') 
            {
                n++;
            }
        }
        Info<< "Number of Species/ "<< n << endl;

        // Create species concentration fields
        PtrList<volScalarField> C(n);
        PtrList<dimensionedScalar> CiAve(n);
        for (label i=0; i < C.size(); i++)
        {
            word fieldName = "C" + Foam::name(i);
            Info<< "Reading field " << fieldName << endl;

            C.set 
            (
                i,
                new volScalarField
                (
                    IOobject
                    (
                        fieldName,
                        runTime.timeName(),
                        mesh,
                        IOobject::MUST_READ,
                        IOobject::AUTO_WRITE
                    ),
                    mesh
                )
            );

            word fieldName2 = "CiAve" + Foam::name(i);
            Info<< "Creating field" << fieldName2 << endl;

            CiAve.set
            (
                i,
                new dimensionedScalar
                (
                    fieldName2,
                    dimensionSet(0,0,0,0,0),
                    scalar(0.)
                )
            );
        }

        if (Uheader.headerOk() && alphaHeader.headerOk())
        {
            Info<< "    Reading U" << endl;
            volVectorField U(Uheader, mesh);

            Info<< "    Reading alpha1" << endl;
            volScalarField alpha1(alphaHeader, mesh);

            dimensionedVector sumField("sumField",dimensionSet(0,0,0,0,0),Foam::vector(0,0,0));
            dimensionedVector Ugas("Ugas",dimensionSet(0,1,-1,0,0),Foam::vector(0,0,0));
            dimensionedVector Uliq("Uliq",dimensionSet(0,1,-1,0,0),Foam::vector(0,0,0));

            if (!isMRF)
            {
                Info<< "not MRF " << endl;
                volScalarField alpha2 = scalar(1.) - alpha1;

                Uliq = U.weightedAverage(alpha2*mesh.Vsc());
                Ugas = U.weightedAverage(alpha1*mesh.Vsc());

                Info<< " weighted liquid velocity = " << Uliq.value() << 
                       " weighted gas velocity = " << Ugas.value() << endl;
            }
            else
            {
                Info<< "MRF " << endl;
                const label patchI = mesh.boundaryMesh().findPatchID("inlet");
                scalar area = gSum(mesh.magSf().boundaryField()[patchI]);

                Info<< " inlet area: " << area << endl;
                if (area > 0)
                {
                    sumField = gSum
                    (
                        mesh.magSf().boundaryField()[patchI]
                      * U.boundaryField()[patchI]
                    ) / area;
                    Info<< "sumfield dimensions: " << sumField.dimensions() << endl;
                    Info<< " average bubble velocity NEW: " << sumField << endl;
                }
            }

            volScalarField alphaCut = pos(alpha1 - scalar(0.9));

            for (label i=0; i < C.size(); i++)
            {
                CiAve[i] = C[i].weightedAverage(alphaCut*mesh.Vsc());
            }

            dimensionedScalar bubbleArea = sum(mag(fvc::grad(alpha1)) * mesh.V());
            Info<< "bubble volume: " <<  sum(alpha1*mesh.V()).value() << endl;

            if (isMRF)
            {
                fout<< "\t" << runTime.value() << "\t" << sumField.component(1).value() << "\t" << bubbleArea.value();
            }
            else
            {
                fout<< "\t" << runTime.value() << "\t" << Ugas.component(1).value() << "\t" << bubbleArea.value();
            }

            for (label i=0; i < C.size(); i++)
            {
                fout<< "\t" << CiAve[i].value();
            }
            fout<< "\n";

            if (writeResults)
            {
                Info<< "    Calculating velocity Field relative to the bubble motion " << endl;
            }
        }
        else //end if headerOk
        {
            Info<< "    No U" << endl;
        }
    }// end loop time dirs

    Info<< "\nEnd\n" << endl;

    return 0;
}


// ************************************************************************* //

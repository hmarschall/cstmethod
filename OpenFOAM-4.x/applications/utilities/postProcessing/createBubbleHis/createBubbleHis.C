/*---------------------------------------------------------------------------* \
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2011 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Application
    createBubbleHis

Authors
    Daniel Deising     <deising@mma.tu-darmstadt.de>
    All rights reserved.

Description
    Calculates and writes data related to bubble rise. The -noWrite 
    option just outputs the max/min values without writing the field.

    You may refer to this software as :
    //- Deising et. al. 2016, Chem. Eng. Sci. 139, pp. 173-195

    This code has been developed by :
        Daniel Deising (main developer).
    
    Method Development and Intellectual Property :
        Daniel Deising   <deising@mma.tu-darmstadt.de>
        Holger Marschall <marschall@mma.tu-darmstadt.de>
        Dieter Bothe     <bothe@mma.tu-darmstadt.de>

        Mathematical Modeling and Analysis
        Center of Smart Interfaces
        Technische Universitaet Darmstadt
       
    If you use this software for your scientific work or your publications,
    please don't forget to acknowledge explicitly the use of it.

\*---------------------------------------------------------------------------*/

#include "fvc.H"
#include "OFstream.H"
#include "fvCFD.H"
#include "argList.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

int main(int argc, char *argv[])
{
    // add arguments to function call:
    Foam::timeSelector::addOptions();
#   include "addRegionOption.H"
    argList::validOptions.insert("write", "");
    argList::validOptions.insert("mrf", "");

    // create list of time dirs
#   include "setRootCase.H"
#   include "createTime.H"
    instantList timeDirs = timeSelector::select0(runTime, args);
#   include "createNamedMesh.H"

    // read arguments if present
    bool writeResults = args.optionFound("write");
    bool isMRF = args.optionFound("mrf");

    // create/open file bubbleHis.dat
    Info << "wird ausgeführt " << endl;
    ofstream fout("bubbleHis.dat", ios_base::app);
    if (runTime.value() == 0)
    {
        fout<< "\t time \t bubble center position (x,y,z) \t bubble velocity (x,y,z) \t surface area \n" << endl;
    }

    // loop over all time dirs found:
    forAll(timeDirs, timeI)
    {
        runTime.setTime(timeDirs[timeI], timeI);
        Info<< "Time = " << runTime.timeName() << endl;

        IOobject Uheader
        (
            "U",
            runTime.timeName(),
            mesh,
            IOobject::MUST_READ
        );

        IOobject alphaHeader
        (
            "alpha1",
            runTime.timeName(),
            mesh,
            IOobject::MUST_READ
        );

        if (Uheader.headerOk() && alphaHeader.headerOk())
        {
            Info<< "    Reading U" << endl;
            volVectorField U(Uheader, mesh);

            Info<< "    Reading alpha1" << endl;
            volScalarField alpha1(alphaHeader, mesh);

            Info<< "     Calculating bubble position " << endl;
            dimensionedVector bubbleCentre("bubbleCentre",dimLength,vector::zero);
            bubbleCentre = mesh.C().weightedAverage(alpha1*mesh.V());
            Info<< "Bubble centre position: " << bubbleCentre.value() << endl;

            Info<< "     Calculating bubble surface area " << endl;
            dimensionedScalar surfaceArea = sum(mag(fvc::grad(alpha1)) * mesh.V());

            dimensionedVector Urel
            (
                "Urel",
                 dimVelocity,
                vector::zero
            );

            dimensionedVector Ububble("Ububble",Urel);
            dimensionedVector Uwater("Uwater",Urel);

            if (isMRF)
            {
                Info<< "create bubble history for MRF system ..." << endl;

                const label patchI = mesh.boundaryMesh().findPatchID("inlet");
                scalar area = gSum(mesh.magSf().boundaryField()[patchI]);
                Info<< " inlet area: " << area << endl;
                dimensionedVector sumField("sumField",dimensionSet(0,0,0,0,0),Foam::vector(0,0,0));
                if (area > 0)
                {
                    sumField = gSum
                    (
                        mesh.magSf().boundaryField()[patchI]
                      * U.boundaryField()[patchI]
                    ) / area;
                    //Info<< "sumfield dimensions: " << sumField.dimensions() << endl;
                    Info<< " average bubble velocity MRF: " << sumField << endl;
                }
                fout<< "\t" << runTime.value() << "\t" << bubbleCentre.component(0).value() << "\t" 
                    << bubbleCentre.component(1).value() << "\t" << bubbleCentre.component(2).value() << "\t"
                    << sumField.component(0).value() << "\t" << sumField.component(1).value() << "\t"
                    << sumField.component(2).value() << "\t" << surfaceArea.value() << "\n" << endl;
            }
            else
            {
                Ububble = U.weightedAverage(alpha1*mesh.V());
                volScalarField alpha2=scalar(1)-alpha1;
                Uwater = U.weightedAverage(alpha2*mesh.V());
                Urel = Ububble - Uwater;

                Info<< "Average bubble velocity: " << Ububble.value() << endl;
                Info<< "Average water velocity: " << Uwater.value() << endl;
                Info<< "Relative phase velocity: " << Urel.value() << endl;

                fout<< "\t" << runTime.value() << "\t" << bubbleCentre.component(0).value() << "\t"
                    << bubbleCentre.component(1).value() << "\t" << bubbleCentre.component(2).value() << "\t"
                    << Ububble.component(0).value() << "\t" << Ububble.component(1).value() << "\t"
                    << Ububble.component(2).value() << "\t" << surfaceArea.value() << "\n" << endl;
            }

            if (writeResults && !isMRF)
            {
                Info<< "    Calculating velocity Field relative to the bubble motion " << endl;
                volVectorField waterRelVel
                (
                    IOobject
                    (
                        "waterRelVel",
                        runTime.timeName(),
                        mesh,
                        IOobject::NO_READ
                    ),
                    U - Ububble
                );

                Info<< "    Calculating velocity Field relative to the water motion " << endl;
                volVectorField bubbleRelVel
                (
                    IOobject
                    (
                        "bubbleRelVel",
                        runTime.timeName(),
                        mesh,
                        IOobject::NO_READ
                    ),
                    U - Uwater
                );

                waterRelVel.write();
                bubbleRelVel.write();
            }
        } //end if headerOk
        else
        {
            Info<< "    No U" << endl;
        }
    }// end loop time dirs

    Info<< "\nEnd\n" << endl;

    return 0;
}


// ************************************************************************* //

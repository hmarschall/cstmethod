
// Read speciesProperties dictionary
IOdictionary speciesProperties
(
    IOobject
    (
        "speciesProperties",
        runTime.constant(),
        runTime,
        IOobject::MUST_READ,
        IOobject::NO_WRITE
    )
);


// Determine number of species concentration fields
scalar n=0;

// Search for list of objects at startTime
//IOobjectList objects(mesh,"0");  
IOobjectList objects(mesh, runTime.timeName()); // better for restart!

// Search list of objects for volScalarFields
IOobjectList scalarFields
(
    objects.lookupClass("volScalarField")
);

for
(
    IOobjectList::iterator scalarFieldIter 
    = scalarFields.begin();
    scalarFieldIter != scalarFields.end();
    ++ scalarFieldIter
)
{
    // Read field
    volScalarField field
    (
        *scalarFieldIter(),
         mesh
    );
    word fieldname = field.name();
    if (fieldname.find("C")==0 && fieldname.size()<=3)
    {
        n++;
    }
}
Info<< "Number of Species/ "<< n << endl;


// get average bubble concentration
PtrList<dimensionedScalar> CiAve(n);
PtrList<dimensionedScalar> CiAveOld(n);
PtrList<dimensionedScalar> Sh(n);

for (label i=0; i < n; i++)
{
    word fieldName = "CiAve" + Foam::name(i);
    CiAve.set
    (
        i,
        new dimensionedScalar
        (
            fieldName,
            dimensionSet(0,0,0,0,0),
            scalar(1.)
        )
    );

    fieldName = "CiAveOld" + Foam::name(i);
    CiAveOld.set
    (
        i,
        new dimensionedScalar
        (
            fieldName,
            dimensionSet(0,0,0,0,0),
            scalar(1.)
        )
    );

    fieldName = "Sh" + Foam::name(i);
    Sh.set
    (
        i,
        new dimensionedScalar
        (
            fieldName,
            dimensionSet(0,0,0,0,0),
            scalar(0.)
        )
    );
}


mesh.readUpdate();

IOobject Uheader
(
    "U",
    runTime.timeName(),
    mesh,
    IOobject::MUST_READ
);

IOobject alphaHeader
(
    "alpha1",
    runTime.timeName(),
    mesh,
    IOobject::MUST_READ
);


//if (Uheader.headerOk() && alphaHeader.headerOk())
//{
    Info<< "    Reading U" << endl;
    volVectorField U(Uheader, mesh);

    Info<< "    Reading alpha1" << endl;
    volScalarField alpha1(alphaHeader, mesh);
//}
//else
//{
//    return 0;
//}

// get etaF
#include "createPhi.H"

Info<< "Reading transportProperties\n" << endl;
TwoPhaseMixture twoPhaseProperties(U, phi);

IOdictionary transportProperties
(
    IOobject
    (
        "transportProperties",
        runTime.constant(),
        runTime,
        IOobject::MUST_READ,
        IOobject::NO_WRITE
    )
);

const surfaceScalarField& etaF = twoPhaseProperties.etaf();

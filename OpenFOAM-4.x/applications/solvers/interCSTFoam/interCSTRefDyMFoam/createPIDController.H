
IOdictionary mrfProperties
(
    IOobject
    (
        "mrfProperties",
        runTime.constant(),
        runTime,
        IOobject::MUST_READ_IF_MODIFIED,
        IOobject::NO_WRITE
    )
);

// set current PID-error
dimensionedVector errorValue
(
    "errorValue",
    sError
);
Info<< "PID-Error = " << errorValue.value() << endl;

// set current PID integral corrector
dimensionedVector integralComponent
(
    "integralComponent",
    sIntegralComponent
);
Info<< "PID-Int.Corrector = " << integralComponent.value() << endl;

// read PID controller settings:
dimensionedScalar KP
(
    mrfProperties.subDict("pidController").lookup("KP")
);

dimensionedScalar KI
(
    mrfProperties.subDict("pidController").lookup("KI")
);

dimensionedScalar KD
(
    mrfProperties.subDict("pidController").lookup("KD")
);


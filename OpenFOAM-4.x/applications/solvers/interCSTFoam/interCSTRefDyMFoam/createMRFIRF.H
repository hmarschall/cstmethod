
    #include "addFieldsMRFIRF.H"  //-DD : init variables needed for MRF/IRF
    #include "createPIDController.H"

//-------------------------------------------
//---- initialize MRF/IRF -------------------
//-------------------------------------------
    // set UAdjustMask: needed to distinguish between 2D and 3D case 
    // and symmetry and no-symmetry case
    // the value of UAdjustMask decides in which directions (x,y,z) a velocity correction will be performed
    vector UAdjustMask = vector::one;

    forAll (mesh.boundaryMesh(), patchi)
    {
        word BCname = mesh.boundaryMesh().names()[patchi];
        word BCtype = mesh.boundaryMesh().types()[patchi];
        Info<< "BCname: "<< BCname << ", BCtype: " << BCtype << endl;
        if (BCtype != "symmetry" && BCtype != "symmetryPlane" && BCtype != "empty")
        {
            // here is missing an if-test which tests for symmetric boundaries ...
            forAll (mesh.Sf().boundaryField()[patchi], facei)
            {
                vector n = mesh.Sf().boundaryField()[patchi][facei]/mesh.magSf().boundaryField()[patchi][facei];
                UAdjustMask -= n*(n & UAdjustMask);
            }
        }
    }

    UAdjustMask = vector::one - UAdjustMask;
    if (mrfProperties.subDict("pidController").found("UAdjustMask"))
    {
        Info << "Reading UAdjustMask from dictionary ... " << endl;
        UAdjustMask = vector(mrfProperties.subDict("pidController").lookup("UAdjustMask"));
    }
    Info << "UAdjustMask = " << UAdjustMask << endl;


    // get ID of patch with name "inlet"
    label inletPatchLabel = -1;
    bool adjustInlet = false;
    forAll(mesh.boundary(), patchi)
    {
        if (mesh.boundary()[patchi].name() == "inlet" &&
            mesh.boundary()[patchi].size() != 0)
        {
            inletPatchLabel = patchi;
            adjustInlet = true;
            Info<< "inletPatchID: " << inletPatchLabel << endl;
            break;
        }
    }
    reduce(adjustInlet, orOp<bool>());

    // Calculate size of inlet patch
    scalar magAreasSum = 0;
    if (inletPatchLabel >= 0)
    {
        magAreasSum = sum(mesh.magSf().boundaryField()[inletPatchLabel]);
    }
    reduce(magAreasSum, sumOp<scalar>());

    // Switch if MRF or IRF should be used
    word mrfModel("IRF");   // use MRF as standard
    if (mrfProperties.found("movingFrame"))
    {
        mrfModel = mrfProperties.subDict("movingFrame")["movingFrameModel"][0].wordToken();
        Info<< "Using moving reference type: " << mrfModel << " for simulation ... " << endl;
    }


// for cases where symmetryPlane cuts the bubble
    vector bubbleCentreAdjust = vector::one;
    if (mrfProperties.subDict("pidController").found("bubbleCentreAdjust"))
    {
        bubbleCentreAdjust = vector(mrfProperties.subDict("pidController").lookup("bubbleCentreAdjust"));
    }


//-------------------------------------------
//---- setup variables needed for MRF/IRF ---
//-------------------------------------------
    // correct centreTarget and centreCurrent for wedges/symmetryPlanes
    sCentreTarget.value() = cmptMultiply(sCentreTarget.value(),bubbleCentreAdjust);
    sCentre.value() = cmptMultiply(sCentre.value(),bubbleCentreAdjust);
    
    // set target bubble centre position
    dimensionedVector centreTarget
    (
        "centreTarget",
        sCentreTarget
    );

    // set current bubble centre position
    dimensionedVector centreCurrent
    (
        "centre",
        sCentre
    );
    Info<< "Expected bubble centre = " << centreTarget.value() << endl;
    Info<< "Current bubble centre = " << centreCurrent.value() << endl;

    Info<< "Reading field frameVel\n" << endl;
    volVectorField frameVel
    (
        IOobject
        (
            "frameVel",
            runTime.timeName(),
            mesh,
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
        mesh, 
        dimensionedVector("frameVel", sInletVel)
    );

    surfaceScalarField phiFrameVel("phiFrameVel", linearInterpolate(frameVel) & mesh.Sf());

    surfaceScalarField rhoPhiFrameVel("rhoPhiFrameVel", rho1*phiFrameVel);

    dimensionedVector inletVel
    (
        "inletVel",
        sInletVel
    );

    dimensionedVector bubbleVel
    (
        "bubbleVel",
        sBubbleVel
    );

    dimensionedVector bubbleRelVel
    (
        "bubbleRelVel",
        sBubbleRelVel
    );

    dimensionedVector UAdjust
    (
        "UAdjust",
        sUAdjust
    ); 
    Info<< "Adjustment-Value = " << UAdjust.value() << endl;

    dimensionedVector frameAcc
    (
        "frameAcc",
        sFrameAcc
    );

    dimensionedVector framePos
    (
        "framePos",
        sFramePos
    );
    Info<< "framePos = " << framePos.value() << endl;

    dimensionedScalar SMALLV
    (
        "SMALLV",
        dimVelocity,
        scalar(SMALL)
    );

    dimensionedScalar SMALLM
    (
        "SMALLM",
        dimless/dimLength,
        scalar(SMALL)
    );

    // Read vorticity vector and calculate shear rate
    dimensionedVector omega
    (
        mrfProperties.subDict("movingFrame").lookup("omega")
    );
    Info << "omega = " << omega.value() << endl;

    dimensionedVector shearU = omega ^ vector(1, 0, 0);
    Info << "shear rate = " << shearU.value() << endl;


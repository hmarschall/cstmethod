// call to mesh.update() is the same for all DyM-solvers
scalar timeBeforeMeshUpdate = runTime.elapsedCpuTime();
{
    // Do any mesh changes
    mesh.update();
}

if (mesh.changing())
{
    Info<< "Execution time for mesh.update() = "
        << runTime.elapsedCpuTime() - timeBeforeMeshUpdate
        << " s" << endl;

    gh = g & mesh.C();
    ghf = g & mesh.Cf();
}

if (mesh.changing() && correctPhi)
{
    // calculate absolute flux from the mapped face velocity
    Info<<"max flux divergence after phi-mapping: " << max(mag(fvc::div(phi))) << endl;
    phi = mesh.Sf() & Uf;
    
    if (mesh.isBalanced())
    {
        Info<<"load balancing was called!" << endl;
        phi = mesh.Sf() & fvc::interpolate(U);
    }
    Info<<"max flux divergence before flux correction: " << max(mag(fvc::div(phi))) << endl;
    #include "correctPhiOld.H"
    Info<<"max flux divergence after flux correction: " << max(mag(fvc::div(phi))) << endl;
}

if (mesh.changing() && checkMeshCourantNo)
{
    #include "meshCourantNo.H"
}

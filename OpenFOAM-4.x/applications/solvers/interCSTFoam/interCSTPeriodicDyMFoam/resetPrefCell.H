
//- set pRefCell to cell containing centre-of-mass of phase 1
    //- compute phase 1 barycentre
    //vector barycentreAlpha1 = mesh.C().weightedAverage(alpha1*mesh.V()).value();
    
    //vector barycentreAlpha1 = transportProperties.subDict("periodicBox").lookup("initBarycentre");
    
    //- advect barycentre
    barycentreAlpha1 = barycentreAlpha1 + (runTime.deltaT()*U[pRefCell]).value();
    Info<< " new barycentre position : " << barycentreAlpha1 << endl;

    //- get cell label of cell containing barycentre-point
    pRefCell = mesh.findCell(barycentreAlpha1);
//scalar pRefValue = p_rgh[pRefCell].value();

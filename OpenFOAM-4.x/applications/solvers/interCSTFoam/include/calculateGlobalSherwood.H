
// calculate the average species concentration inside the bubble
// this is needed to calculate the global Sherwood number
    for (label i=0; i < C.size(); i++)
    {
        CiAveOld[i] = CiAve[i];

        // get bubble by iso-contour alpha=0.9 (to get the gas-sided part of the concentration)
        CiAve[i] = C[i].weightedAverage(pos(alpha1 - scalar(0.9))()*mesh.Vsc());
    }

// calculate bubble interfacial area:
// NOTE: gSum strips dimensions: bubbleArea is dimensionless!!!
    dimensionedScalar bubbleArea = gSum(mag(fvc::grad(alpha1)) * mesh.V());

//---------------------------------------------//
// calculate the global Sherwood number
// based on concentration loss of bubble:

// calculate bubble volume:
// NOTE: gSum strips dimensions: bubbleVolume is dimensionless!!!
    dimensionedScalar bubbleVolume("bubbleVolume", pos(alpha1 - scalar(0.9))().weightedAverage(mesh.V()) * gSum(mesh.V()));

// loop over all species:
    for (label i=0; i <C.size(); i++)
    {
    //calculate Ci dot (time derivative of average bubble concentration)
        dimensionedScalar deltaCi = CiAveOld[i] - CiAve[i];
        dimensionedScalar ddtCi = deltaCi/runTime.deltaT();
    
    // calculate liquid-sided concentration at interface
        // version 1: based on average species concentration in the bubble (only valid for Dgas >> Dliq)
        dimensionedScalar CiIliq = (CiAveOld[i] + CiAve[i]);
        if (He[i].value() > 0)
        {
            CiIliq = CiIliq / (2.0*He[i]);
        }

/*
        // UNTESTED:
        // version 2: get bubble-sided interface concentration from bubble-sided interface cells
        // get cells at bubble-side of interface
        volScalarField nearInterface = pos(alpha1-0.01) * pos(0.99-alpha1);
        for (int count=0; count<2; count++)
        {
            nearInterface = pos(fvc::average(linearInterpolate(nearInterface)) - 0.01);
        }
        volScalarField isBubble = pos(alpha1-0.99);
        volScalarField markCells = pos(isBubble * nearInterface - 0.01);

        // create scalar for compression flux scaling
        dimensionedScalar averageCiBubble("averageCiBubble",dimensionSet(0,0,0,0,0),1.);

        // get average concentration at bubble-side of interface (\cav{c}{\varphi})
        // if-test is needed in the first few time-steps when starting with a sharp field
        if (max(markCells).value() > 0)
        {
            averageCiBubble = C[i].weightedAverage(markCells*mesh.V());
            Info<<"bubble-sided interface concentration: " << averageCiBubble.value() << endl;
        }
        else
        {
            averageCiBubble = C[i].weightedAverage(isBubble*mesh.V());
            Info<<"bubble-sided interface concentration: " << averageCiBubble.value() << endl;
        }

        dimensionedScalar CiIliq = averageCiBubble/He[i];
*/
    //bubbleVolume = 1.2566e-05;
    //bubbleArea = 1.2566e-02;

    // calculate global Sherwood number (gSum strips dimensions => correct dimensions with factor)
        if (Dliq[i].value() > 0 && CiIliq.value() > 0)
        {
            Sh[i] = (ddtCi/CiIliq * bubbleVolume/bubbleArea) * bubbleDiam / Dliq[i] * dimensionedScalar("fac", dimLength, 1.);
        }
        else
        {
            Sh[i] = 0;
        }
        Info<< "Sherwood global (C" << i << ") = " << Sh[i].value() << endl;
    }

if (Pstream::master())
{
    // write global Sherwood number and average species concentration to file:
    ofstream writeCi("speciesData.dat", ios_base::app);

    if (runTime.value() == 0)
    {
        writeCi<< "\t time \t bubble area \t bubble volume \t average species concentrations Ci \t global Sherwood \n" << endl;
    }

    writeCi<< "\t" << runTime.value() << "\t" << bubbleArea.value() << "\t" << bubbleVolume.value();
    for (label i=0; i < C.size(); i++)
    {
        writeCi<< "\t" << CiAve[i].value() << "\t" << Sh[i].value();
    }
    writeCi<< "\n" << endl;
}

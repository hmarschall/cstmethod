#!/bin/bash

cd ${0%/*} || exit 1    # run from this directory

. etc/bashrc            # source the bashrc to set additional env-variables

# compile Interface Capturing Schemes - Library
cd $CST_PROJECT/src/interfaceCapturingSchemes
wmake libso

# compile additional HR schemes
cd $CST_PROJECT/src/HiresSchemes
wmake libso

# compile reconstruction and curvature models
cd $CST_PROJECT/src/transportModels/interface
wmake libso

# compile twoPhaseMixture
cd $CST_PROJECT/src/transportModels/twoPhaseMixture
wmake libso

# compile finiteVolume class additions
# contains implicit skewness correction schemes, modified MULES solver
# and CST model convectionSchemes
cd $CST_PROJECT/src/finiteVolume
wmake libso

# compile speciesTransferModel (CST model library)
cd $CST_PROJECT/src/transportModels/speciesTransferModel
wmake libso

# compile reactiveMixture (chemical reaction library)
cd $CST_PROJECT/src/transportModels/reactiveMixture
wmake libso

# compile loadBalancing library
cd $CST_PROJECT/loadBalancing/src
wmake libso dynamicMesh
wmake libso dynamicFvMesh
wmake libso dynamicRefineBalancedFvMesh
wmake libso userDecompositionMethods

cd $CST_PROJECT/loadBalancing/utilities
wmake decomposeParLevel
wmake reconstructParLevel
wmake execRefinement
wmake testUtil

# compile solvers
cd $CST_PROJECT/applications/solvers/interCSTFoam
./Allwmake

# compile pre processing tools
cd $CST_PROJECT/applications/utilities/preProcessing
wmake initField
wmake initConcentration

# compile post processing tools
cd $CST_PROJECT/applications/utilities/postProcessing
wmake createBubbleHis
wmake calculateAverageBubbleSpeciesConcentration
wmake calcGlobalSherwoodFromAverageConcentration


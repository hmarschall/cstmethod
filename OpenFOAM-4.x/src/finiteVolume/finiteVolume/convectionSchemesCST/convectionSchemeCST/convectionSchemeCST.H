/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2011-2016 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::fv::convectionSchemeCST

SourceFiles
    convectionSchemeCST.C
    convectionSchemesCST.C

Author
    Daniel Deising     <deising@mma.tu-darmstadt.de>
    All rights reserved.

Description
    Abstract base class for convection schemes. Modified for the
    implicit discretization of the CST model. Adds one flux field (fluxInterp),
    which is used to distinguish between upwind and downwind cell.

    You may refer to this software as :
    //- full bibliographic data to be provided

    This code has been developed by :
        Daniel Deising (main developer).
    
    Method Development and Intellectual Property :
        Daniel Deising   <deising@mma.tu-darmstadt.de>
        Holger Marschall <marschall@mma.tu-darmstadt.de>
        Dieter Bothe     <bothe@mma.tu-darmstadt.de>

        Mathematical Modeling and Analysis
        Center of Smart Interfaces
        Technische Universitaet Darmstadt
       
    If you use this software for your scientific work or your publications,
    please don't forget to acknowledge explicitly the use of it.

\*---------------------------------------------------------------------------*/

#ifndef convectionSchemeCST_H
#define convectionSchemeCST_H

#include "tmp.H"
#include "volFieldsFwd.H"
#include "surfaceFieldsFwd.H"
#include "typeInfo.H"
#include "runTimeSelectionTables.H"
#include "multivariateSurfaceInterpolationScheme.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

template<class Type>
class fvMatrix;

class fvMesh;

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace fv
{

/*---------------------------------------------------------------------------*\
                           Class convectionSchemeCST Declaration
\*---------------------------------------------------------------------------*/

template<class Type>
class convectionSchemeCST
:
    public tmp<convectionSchemeCST<Type>>::refCount
{
    // Private data

        const fvMesh& mesh_;


public:

    //- Runtime type information
    virtual const word& type() const = 0;


    // Declare run-time constructor selection tables

        declareRunTimeSelectionTable
        (
            tmp,
            convectionSchemeCST,
            Istream,
            (
                const fvMesh& mesh,
                const surfaceScalarField& faceFlux,
                const surfaceScalarField& fluxInterp,
                Istream& schemeData
            ),
            (mesh, faceFlux, fluxInterp, schemeData)
        );

        declareRunTimeSelectionTable
        (
            tmp,
            convectionSchemeCST,
            Multivariate,
            (
                const fvMesh& mesh,
                const typename multivariateSurfaceInterpolationScheme<Type>::
                    fieldTable& fields,
                const surfaceScalarField& faceFlux,
                const surfaceScalarField& fluxInterp,
                Istream& schemeData
            ),
            (mesh, fields, faceFlux, fluxInterp, schemeData)
        );


    // Constructors

        //- Copy construct
        convectionSchemeCST(const convectionSchemeCST&);

        //- Construct from mesh, flux and Istream
        convectionSchemeCST
        (
            const fvMesh& mesh,
            const surfaceScalarField&,
            const surfaceScalarField& // added by DD
        )
        :
            mesh_(mesh)
        {}


    // Selectors

        //- Return a pointer to a new convectionSchemeCST created on freestore
        static tmp<convectionSchemeCST<Type>> New
        (
            const fvMesh& mesh,
            const surfaceScalarField& faceFlux,
            const surfaceScalarField& fluxInterp, // added by DD
            Istream& schemeData
        );


        //- Return a pointer to a new multivariate convectionSchemeCST
        //  created on freestore
        static tmp<convectionSchemeCST<Type>> New
        (
            const fvMesh& mesh,
            const typename multivariateSurfaceInterpolationScheme<Type>::
                fieldTable& fields,
            const surfaceScalarField& faceFlux,
            const surfaceScalarField& fluxInterp, // added by DD
            Istream& schemeData
        );


    //- Destructor
    virtual ~convectionSchemeCST();


    // Member Functions

        //- Return mesh reference
        const fvMesh& mesh() const
        {
            return mesh_;
        }

        virtual tmp<GeometricField<Type, fvsPatchField, surfaceMesh>>
        interpolate
        (
            const surfaceScalarField&,
            const GeometricField<Type, fvPatchField, volMesh>&
        ) const = 0;

        virtual tmp<GeometricField<Type, fvsPatchField, surfaceMesh>> flux
        (
            const surfaceScalarField&,
            const GeometricField<Type, fvPatchField, volMesh>&
        ) const = 0;

        virtual tmp<fvMatrix<Type>> fvmDiv
        (
            const surfaceScalarField&,
            const GeometricField<Type, fvPatchField, volMesh>&
        ) const = 0;

        virtual tmp<GeometricField<Type, fvPatchField, volMesh>> fvcDiv
        (
            const surfaceScalarField&,
            const GeometricField<Type, fvPatchField, volMesh>&
        ) const = 0;
        
// added by DD:
        virtual tmp<fvMatrix<Type> > fvmDivCST
        (
            const surfaceScalarField&,
            const surfaceScalarField&,
            const GeometricField<Type, fvPatchField, volMesh>&
        ) const = 0;

        virtual tmp<GeometricField<Type, fvPatchField, volMesh> > fvcDivCST
        (
            const surfaceScalarField&,
            const surfaceScalarField&,
            const GeometricField<Type, fvPatchField, volMesh>&
        ) const = 0;


    // Member operators

        void operator=(const convectionSchemeCST<Type>&);
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace fv

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// Add the patch constructor functions to the hash tables

#define makeFvConvectionTypeSchemeCST(SS, Type)                                   \
    defineNamedTemplateTypeNameAndDebug(Foam::fv::SS<Foam::Type>, 0);          \
                                                                               \
    namespace Foam                                                             \
    {                                                                          \
        namespace fv                                                           \
        {                                                                      \
            convectionSchemeCST<Type>::addIstreamConstructorToTable<SS<Type>>     \
                add##SS##Type##IstreamConstructorToTable_;                     \
        }                                                                      \
    }

#define makeFvConvectionSchemeCST(SS)                                             \
                                                                               \
makeFvConvectionTypeSchemeCST(SS, scalar)                                         \
makeFvConvectionTypeSchemeCST(SS, vector)                                         \
makeFvConvectionTypeSchemeCST(SS, sphericalTensor)                                \
makeFvConvectionTypeSchemeCST(SS, symmTensor)                                     \
makeFvConvectionTypeSchemeCST(SS, tensor)


#define makeMultivariateFvConvectionTypeSchemeCST(SS, Type)                       \
    defineNamedTemplateTypeNameAndDebug(Foam::fv::SS<Foam::Type>, 0);          \
                                                                               \
    namespace Foam                                                             \
    {                                                                          \
        namespace fv                                                           \
        {                                                                      \
            convectionSchemeCST<Type>::                                           \
                addMultivariateConstructorToTable<SS<Type>>                    \
                add##SS##Type##MultivariateConstructorToTable_;                \
        }                                                                      \
    }


#define makeMultivariateFvConvectionSchemeCST(SS)                                 \
                                                                               \
makeMultivariateFvConvectionTypeSchemeCST(SS, scalar)                             \
makeMultivariateFvConvectionTypeSchemeCST(SS, vector)                             \
makeMultivariateFvConvectionTypeSchemeCST(SS, sphericalTensor)                    \
makeMultivariateFvConvectionTypeSchemeCST(SS, symmTensor)                         \
makeMultivariateFvConvectionTypeSchemeCST(SS, tensor)


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#ifdef NoRepository
    #include "convectionSchemeCST.C"
#endif

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //

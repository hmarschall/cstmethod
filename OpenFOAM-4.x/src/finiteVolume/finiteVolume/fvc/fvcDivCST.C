/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2011 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "fvcDivCST.H"
#include "fvMesh.H"
#include "fvcSurfaceIntegrate.H"
#include "divScheme.H"
#include "convectionScheme.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace fvc
{

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

template<class Type>
tmp<GeometricField<Type, fvPatchField, volMesh> >
divCST
(
    const GeometricField<Type, fvsPatchField, surfaceMesh>& ssf
)
{
    return tmp<GeometricField<Type, fvPatchField, volMesh> >
    (
        new GeometricField<Type, fvPatchField, volMesh>
        (
            "div("+ssf.name()+')',
            fvc::surfaceIntegrate(ssf)
        )
    );
}


template<class Type>
tmp<GeometricField<Type, fvPatchField, volMesh> >
divCST
(
    const tmp<GeometricField<Type, fvsPatchField, surfaceMesh> >& tssf
)
{
    tmp<GeometricField<Type, fvPatchField, volMesh> > Div(fvc::divCST(tssf()));
    tssf.clear();
    return Div;
}


template<class Type>
tmp
<
    GeometricField
    <
        typename innerProduct<vector, Type>::type, fvPatchField, volMesh
    >
>
divCST
(
    const GeometricField<Type, fvPatchField, volMesh>& vf,
    const word& name
)
{
    return fv::divScheme<Type>::New
    (
        vf.mesh(), vf.mesh().divScheme(name)
    )().fvcDiv(vf);
}


template<class Type>
tmp
<
    GeometricField
    <
        typename innerProduct<vector, Type>::type, fvPatchField, volMesh
    >
>
divCST
(
    const tmp<GeometricField<Type, fvPatchField, volMesh> >& tvvf,
    const word& name
)
{
    typedef typename innerProduct<vector, Type>::type DivType;
    tmp<GeometricField<DivType, fvPatchField, volMesh> > Div
    (
        fvc::divCST(tvvf(), name)
    );
    tvvf.clear();
    return Div;
}

template<class Type>
tmp
<
    GeometricField
    <
        typename innerProduct<vector, Type>::type, fvPatchField, volMesh
    >
>
divCST
(
    const GeometricField<Type, fvPatchField, volMesh>& vf
)
{
    return fvc::divCST(vf, "div("+vf.name()+')');
}


template<class Type>
tmp
<
    GeometricField
    <
        typename innerProduct<vector, Type>::type, fvPatchField, volMesh
    >
>
divCST
(
    const tmp<GeometricField<Type, fvPatchField, volMesh> >& tvvf
)
{
    typedef typename innerProduct<vector, Type>::type DivType;
    tmp<GeometricField<DivType, fvPatchField, volMesh> > Div(fvc::divCST(tvvf()));
    tvvf.clear();
    return Div;
}


template<class Type>
tmp<GeometricField<Type, fvPatchField, volMesh> >
divCST
(
    const surfaceScalarField& flux,
    const surfaceScalarField& fluxInterp,
    const GeometricField<Type, fvPatchField, volMesh>& vf,
    const word& name
)
{
    return fv::convectionSchemeCST<Type>::New
    (
        vf.mesh(),
        flux,
        fluxInterp,
        vf.mesh().divScheme(name)
    )().fvcDivCST(flux, fluxInterp, vf);
}

template<class Type>
tmp<GeometricField<Type, fvPatchField, volMesh> >
divCST
(
    const tmp<surfaceScalarField>& tflux,
    const surfaceScalarField& fluxInterp,
    const GeometricField<Type, fvPatchField, volMesh>& vf,
    const word& name
)
{
    tmp<GeometricField<Type, fvPatchField, volMesh> > Div
    (
        fvc::divCST(tflux(), fluxInterp, vf, name)
    );
    tflux.clear();
    return Div;
}

template<class Type>
tmp<GeometricField<Type, fvPatchField, volMesh> >
divCST
(
    const surfaceScalarField& flux,
    const tmp<surfaceScalarField>& tfluxInterp,
    const GeometricField<Type, fvPatchField, volMesh>& vf,
    const word& name
)
{
    tmp<GeometricField<Type, fvPatchField, volMesh> > Div
    (
        fvc::divCST(flux, tfluxInterp(), vf, name)
    );
    tfluxInterp.clear();
    return Div;
}


template<class Type>
tmp<GeometricField<Type, fvPatchField, volMesh> >
divCST
(
    const tmp<surfaceScalarField>& tflux,
    const tmp<surfaceScalarField>& tfluxInterp,
    const GeometricField<Type, fvPatchField, volMesh>& vf,
    const word& name
)
{
    tmp<GeometricField<Type, fvPatchField, volMesh> > Div
    (
        fvc::divCST(tflux(), tfluxInterp(), vf, name)
    );
    tflux.clear();
    tfluxInterp.clear();
    return Div;
}


template<class Type>
tmp<GeometricField<Type, fvPatchField, volMesh> >
divCST
(
    const surfaceScalarField& flux,
    const surfaceScalarField& fluxInterp,
    const tmp<GeometricField<Type, fvPatchField, volMesh> >& tvf,
    const word& name
)
{
    tmp<GeometricField<Type, fvPatchField, volMesh> > Div
    (
        fvc::divCST(flux, fluxInterp, tvf(), name)
    );
    tvf.clear();
    return Div;
}


template<class Type>
tmp<GeometricField<Type, fvPatchField, volMesh> >
divCST
(
    const tmp<surfaceScalarField>& tflux,
    const tmp<surfaceScalarField>& tfluxInterp,
    const tmp<GeometricField<Type, fvPatchField, volMesh> >& tvf,
    const word& name
)
{
    tmp<GeometricField<Type, fvPatchField, volMesh> > Div
    (
        fvc::divCST(tflux(), tfluxInterp(), tvf(), name)
    );
    tflux.clear();
    tfluxInterp.clear();
    tvf.clear();
    return Div;
}


template<class Type>
tmp<GeometricField<Type, fvPatchField, volMesh> >
divCST
(
    const surfaceScalarField& flux,
    const surfaceScalarField& fluxInterp,
    const GeometricField<Type, fvPatchField, volMesh>& vf
)
{
    return fvc::divCST
    (
        flux, fluxInterp, vf, "div("+flux.name()+','+vf.name()+')'
    );
}


template<class Type>
tmp<GeometricField<Type, fvPatchField, volMesh> >
divCST
(
    const tmp<surfaceScalarField>& tflux,
    const tmp<surfaceScalarField>& tfluxInterp,
    const GeometricField<Type, fvPatchField, volMesh>& vf
)
{
    tmp<GeometricField<Type, fvPatchField, volMesh> > Div
    (
        fvc::divCST(tflux(), tfluxInterp(), vf)
    );
    tflux.clear();
    tfluxInterp.clear();
    return Div;
}


template<class Type>
tmp<GeometricField<Type, fvPatchField, volMesh> >
divCST
(
    const surfaceScalarField& flux,
    const surfaceScalarField& fluxInterp,
    const tmp<GeometricField<Type, fvPatchField, volMesh> >& tvf
)
{
    tmp<GeometricField<Type, fvPatchField, volMesh> > Div
    (
        fvc::divCST(flux, fluxInterp, tvf())
    );
    tvf.clear();
    return Div;
}


template<class Type>
tmp<GeometricField<Type, fvPatchField, volMesh> >
divCST
(
    const tmp<surfaceScalarField>& tflux,
    const tmp<surfaceScalarField>& tfluxInterp,
    const tmp<GeometricField<Type, fvPatchField, volMesh> >& tvf
)
{
    tmp<GeometricField<Type, fvPatchField, volMesh> > Div
    (
        fvc::divCST(tflux(), tfluxInterp(), tvf())
    );
    tflux.clear();
    tfluxInterp.clear();
    tvf.clear();
    return Div;
}


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace fvc

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// ************************************************************************* //

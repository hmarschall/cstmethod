/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2011 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "IrreversibleReaction.H"

// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

template<class ReactionRate>
Foam::IrreversibleReaction<ReactionRate>::IrreversibleReaction
(
    const Reaction& reaction,
    const ReactionRate& k
)
:
    Reaction(reaction),
    k_(k)
{}


template<class ReactionRate>
Foam::IrreversibleReaction<ReactionRate>::IrreversibleReaction
(
    const speciesTable& species,
    Istream& is
)
:
    Reaction(species, is),
    k_(species, is)
{}


template<class ReactionRate>
Foam::IrreversibleReaction<ReactionRate>::IrreversibleReaction
(
    const speciesTable& species,
    const dictionary& dict
)
:
    Reaction(species, dict),
    k_(species, dict)
{}


template<class ReactionRate>
Foam::IrreversibleReaction<ReactionRate>::IrreversibleReaction
(
    const IrreversibleReaction<ReactionRate>& irr,
    const speciesTable& species
)
:
    Reaction(irr, species),
    k_(irr.k_)
{}


// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

template<class ReactionRate>
Foam::scalar Foam::IrreversibleReaction<ReactionRate>::kf
(
    const scalar T,
    const scalar p,
    const scalarField& c
) const
{
    return k_(T, p, c);
}


template<class ReactionRate>
void Foam::IrreversibleReaction<ReactionRate>::write
(
    Ostream& os
) const
{
    Reaction::write(os);
    k_.write(os);
}


// ************************************************************************* //

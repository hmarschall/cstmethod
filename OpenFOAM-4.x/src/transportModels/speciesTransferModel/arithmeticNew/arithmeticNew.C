/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2011 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "arithmeticNew.H"
#include "addToRunTimeSelectionTable.H"
#include "surfaceFields.H"
#include "fvc.H"
#include "fvmDivCST.H"

// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //

namespace Foam
{
namespace cstModels
{
    defineTypeNameAndDebug(arithmeticNew, 0);
    addToRunTimeSelectionTable(cstModel, arithmeticNew, dictionary);
}
}


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::cstModels::arithmeticNew::arithmeticNew
(
    const word& name,
    const dictionary& speciesProperties,
    const volScalarField& alpha,
    const surfaceScalarField& etaf
)
:
    cstModel(name, speciesProperties, alpha, etaf),
    cstFluxes_(nFluxes_)
{
    // initialize pointers with zero
    for(label i = 0; i< nFluxes_; i++)
    {
        cstFluxes_[i] = NULL;
    }
}


// * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * * //

void Foam::cstModels::arithmeticNew::computeFluxes()
{
// initialize list of flux fields if not yet existing
    for(label i = 0; i< nFluxes_; i++)
    {
        if (!cstFluxes_[i])
        {
            //Info<< "initialize cstFluxes pointer for species " << name_ << endl;
            cstFluxes_[i] = new surfaceScalarField
            (
                IOobject
                (
                    "flux" + Foam::name(i),
                    alpha_.time().timeName(),
                    alpha_.mesh(),
                    IOobject::NO_READ,
                    IOobject::NO_WRITE,
                    false // Do not register
                ),
                alpha_.mesh(),
                dimensionSet(0,1,-1,0,0)
            );
            
            //mesh.objectRegistry::store(cstFluxes_[i]);
        }
    }
    
// compute flux fields
    const dimensionedScalar D1i(this->D1_);
    const dimensionedScalar D2i(this->D2_);
    const dimensionedScalar Hei(this->He_);
    
    volScalarField alpha(max(0.,min(this->alpha_,1.)));
    surfaceScalarField fluxDir = linearInterpolate(-fvc::grad(alpha)) & alpha.mesh().Sf();
    
    const fvMesh& mesh = alpha.mesh();
    
    surfaceScalarField alphaCD(linearInterpolate(alpha));
    surfaceScalarField alphaUpw(upwind<scalar>(mesh, fluxDir).interpolate(alpha));
    surfaceScalarField alphaDow(downwind<scalar>(mesh, fluxDir).interpolate(alpha));
    
    surfaceScalarField snGradAlpha(fvc::snGrad(alpha));
    surfaceScalarField magSf(mesh.magSf());

    volScalarField denominatorV = alpha*Hei + (1.-alpha);
    
    // prevent division by zero in case He=0!!
    if (Hei.value()==0)
    {
        denominatorV += 1e-10;
    }
    
    surfaceScalarField denominator = 
    (
        linearInterpolate(denominatorV)
    );
    surfaceScalarField denominatorUpw =
        upwind<scalar>(mesh, fluxDir).interpolate
        (
            denominatorV
        );
    surfaceScalarField denominatorDow =
        downwind<scalar>(mesh, fluxDir).interpolate
        (
            denominatorV
        );

    *cstFluxes_[0] = 
    (
        (D1i-D2i)
      * snGradAlpha
    ); //* magSf;

    *cstFluxes_[1] = 
    (
        -1/denominatorUpw
      * (D1i*Hei-D2i)
      * snGradAlpha
    ); //* magSf;

    *cstFluxes_[2] = 
    (
        -1/denominatorDow
      * (D1i*Hei-D2i)
      * snGradAlpha
    ); //* magSf;

    *cstFluxes_[3] = 
    (
        (D1i-D2i)*(Hei-1.)/denominator
      * ((1-alphaCD) * (1-alphaUpw) - Hei * alphaCD * alphaUpw)/denominatorUpw
      * snGradAlpha
    ); //* magSf;

    *cstFluxes_[4] = 
    (
        (D1i-D2i)*(Hei-1.)/denominator
      * ((1-alphaCD) * (1-alphaDow) - Hei * alphaCD * alphaDow)/denominatorDow
      * snGradAlpha
    ); //* magSf;
}


Foam::tmp<Foam::surfaceScalarField> Foam::cstModels::arithmeticNew::interfaceFlux()
{
    const volScalarField& Ci(this->Ci_);
    
    volScalarField alpha(max(0.,min(this->alpha_,1.)));
    surfaceScalarField fluxDir = linearInterpolate(-fvc::grad(alpha)) & alpha.mesh().Sf();
    
    const fvMesh& mesh = alpha.mesh();

    // interpolation takes into account mesh grading at the interface
    surfaceScalarField delta = mesh.surfaceInterpolation::weights();
    
    // switch weights when face-parent is not upwind cell!
    forAll(fluxDir, f)
    {
        if (fluxDir[f] < 0)
        {
            delta[f] = 1-delta[f];
        }
    }
    
    computeFluxes();
    
    surfaceScalarField linearFlux = *cstFluxes_[0] * linearInterpolate(Ci);
    
    surfaceScalarField upwindFlux = *cstFluxes_[1] * 
        upwind<scalar>(mesh, fluxDir).interpolate(Ci);

    surfaceScalarField downwindFlux = *cstFluxes_[2] * 
        downwind<scalar>(mesh, fluxDir).interpolate(Ci);
        
    surfaceScalarField upwindFlux2 = *cstFluxes_[3] * 
        upwind<scalar>(mesh, fluxDir).interpolate(Ci);

    surfaceScalarField downwindFlux2 = *cstFluxes_[4] * 
        downwind<scalar>(mesh, fluxDir).interpolate(Ci);

    // re-set cstFluxes to NULL (clean-up)
    for(label i = 0; i< nFluxes_; i++)
    {
        cstFluxes_[i] = NULL;
    }

    return linearFlux + delta*upwindFlux + (1.-delta)*downwindFlux
          + delta*upwindFlux2 + (1.-delta)*downwindFlux2;
}


Foam::tmp<Foam::fvScalarMatrix> Foam::cstModels::arithmeticNew::interfaceTerm()
{
    const volScalarField& Ci(this->Ci_);
    const dimensionedScalar D1i(this->D1_);
    const dimensionedScalar D2i(this->D2_);
    const dimensionedScalar Hei(this->He_);
    
    volScalarField alpha(max(0.,min(this->alpha_,1.)));
    surfaceScalarField fluxDir = linearInterpolate(-fvc::grad(alpha)) & alpha.mesh().Sf();
    
    const fvMesh& mesh = alpha.mesh();

    computeFluxes();

    surfaceScalarField phiDiI = 
    (
        *cstFluxes_[0]
    ) * mesh.magSf();

    surfaceScalarField phiCiIDlinearUpw = 
    (
        *cstFluxes_[1]
    ) * mesh.magSf();

    surfaceScalarField phiCiIDlinearDow = 
    (
        *cstFluxes_[2]
    ) * mesh.magSf();

    surfaceScalarField phiCiIDlinearUpw2 = 
    (
        *cstFluxes_[3]
    ) * mesh.magSf();

    surfaceScalarField phiCiIDlinearDow2 = 
    (
        *cstFluxes_[4]
    ) * mesh.magSf();

    IStringStream upwindScheme("Gauss upwind"); 
    IStringStream downwindScheme("Gauss downwind"); 
    IStringStream linearScheme("Gauss linear"); 

    tmp<Foam::fv::convectionSchemeCST<scalar> > upwScheme(
        Foam::fv::convectionSchemeCST<scalar>::New(
            mesh,
            phiCiIDlinearUpw,
            fluxDir,
            upwindScheme
        )
    );
    tmp<Foam::fv::convectionSchemeCST<scalar> > dowScheme(
        Foam::fv::convectionSchemeCST<scalar>::New(
            mesh,
            phiCiIDlinearDow,
            fluxDir,
            downwindScheme
        )
    );
    tmp<Foam::fv::convectionSchemeCST<scalar> > linScheme(
        Foam::fv::convectionSchemeCST<scalar>::New(
            mesh,
            phiDiI,
            phiDiI,
            linearScheme
        )
    );

    // interpolation takes into account mesh grading at the interface
    surfaceScalarField delta = mesh.surfaceInterpolation::weights();
    
    // switch weights when face-parent is not upwind cell!
    forAll(fluxDir, f)
    {
        if (fluxDir[f] < 0)
        {
            delta[f] = 1-delta[f];
        }
    }
    
    // re-set cstFluxes to NULL (clean-up)
    for(label i = 0; i< nFluxes_; i++)
    {
        cstFluxes_[i] = NULL;
    }

    return (linScheme().fvmDivCST(phiDiI, phiDiI, Ci)
          + upwScheme().fvmDivCST(delta*phiCiIDlinearUpw, fluxDir, Ci)
          + dowScheme().fvmDivCST((1.-delta)*phiCiIDlinearDow, fluxDir, Ci)
          + upwScheme().fvmDivCST(delta*phiCiIDlinearUpw2, fluxDir, Ci)
          + dowScheme().fvmDivCST((1.-delta)*phiCiIDlinearDow2, fluxDir, Ci));
}


Foam::tmp<Foam::surfaceScalarField> Foam::cstModels::arithmeticNew::Dmean()
{
    const dimensionedScalar D1(this->D1_);
    const dimensionedScalar D2(this->D2_);
    const dimensionedScalar He(this->He_);

    volScalarField alpha(max(0.,min(this->alpha_,1.)));
    surfaceScalarField alphaCD(linearInterpolate(alpha));

    //return (linearInterpolate(alpha*D1 + (scalar(1.)-alpha)*D2) 
    //    + (D1-D2)*(1-1/He)*alphaCD*(1-alphaCD)/linearInterpolate(alpha + (1.-alpha)/He));
    
    surfaceScalarField denominator(alphaCD*He + (1.-alphaCD));
    
    // stabilize in case He=0!
    if (He.value()==0)
    {
        denominator += 1e-10;
    }
    
    return (alphaCD*D1 + (scalar(1.)-alphaCD)*D2
        + (D1-D2)*(He-1.)*alphaCD*(1-alphaCD)/denominator);
}


// ************************************************************************* //

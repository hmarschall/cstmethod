/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2011 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "cstModel.H"

// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //

namespace Foam
{
    defineTypeNameAndDebug(cstModel, 0);
    defineRunTimeSelectionTable(cstModel, dictionary);
}


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::cstModel::cstModel
(
    const word& name,
    const dictionary& speciesProperties,
    const volScalarField& alpha,
    const surfaceScalarField& etaf
)
:
    Ci_(alpha.db().lookupObject<volScalarField>(name)),
    name_(name),
    speciesProperties_(speciesProperties),
    alpha_(alpha),
    etaf_(etaf),
    He_(speciesProperties_.lookup("He")),
    D1_(speciesProperties_.lookup("D1")),
    D2_(speciesProperties_.lookup("D2"))
{
    Info<<"read Henry: " << He_ << '\n'
        <<"read D1:    " << D1_ << '\n'
        <<"read D2:    " << D2_ << '\n' << endl;
}


// * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * * //


// ************************************************************************* //


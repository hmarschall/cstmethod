/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2011 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::cstModels::blendedNew

Description
    Use blendedNew mean diffusion coeff to derive one-field CST equation

SourceFiles
    blendedNew.C

\*---------------------------------------------------------------------------*/

#ifndef blendedNew_H
#define blendedNew_H

#include "cstModel.H"
#include "dimensionedScalar.H"
#include "volFieldsFwd.H"
#include "surfaceFieldsFwd.H"
#include "volFields.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{
namespace cstModels
{

/*---------------------------------------------------------------------------*\
                           Class Newtonian Declaration
\*---------------------------------------------------------------------------*/

class blendedNew
:
    public cstModel
{
    //-blendingFactor
    const scalar beta_ = 0.5;

    //- first scheme for blending
    autoPtr<cstModel> tScheme1_;
    
    //- second scheme for blending
    autoPtr<cstModel> tScheme2_;
    

public:

    //- Runtime type information
    TypeName("blendedNew");


    // Constructors

        //- construct from components
        blendedNew
        (
            const word& name,
            const dictionary& speciesProperties,
            const volScalarField& alpha,
            const surfaceScalarField& etaf
        );


    //- Destructor
    ~blendedNew()
    {}


    // Member Functions
        
        //- Return the interfacial species transfer term
        virtual tmp<fvScalarMatrix> interfaceTerm();

        //- Return the mean diffusion coefficient
        virtual tmp<surfaceScalarField> Dmean();
        
        //- Return the interfacial flux field for local Sherwood number calculations
        virtual tmp<surfaceScalarField> interfaceFlux();
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace cstModels
} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //

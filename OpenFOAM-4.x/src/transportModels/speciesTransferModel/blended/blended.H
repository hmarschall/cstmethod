/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2011 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::cstModels::blended

Description
    Use blended mean diffusion coeff to derive one-field CST equation

SourceFiles
    blended.C

\*---------------------------------------------------------------------------*/

#ifndef blended_H
#define blended_H

#include "cstModel.H"
#include "dimensionedScalar.H"
#include "volFieldsFwd.H"
#include "surfaceFieldsFwd.H"
#include "volFields.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{
namespace cstModels
{

/*---------------------------------------------------------------------------*\
                           Class Newtonian Declaration
\*---------------------------------------------------------------------------*/

class blended
:
    public cstModel
{
public:

    //- Runtime type information
    TypeName("blended");


    // Constructors

        //- construct from components
        blended
        (
            const word& name,
            const dictionary& speciesProperties,
            const volScalarField& alpha,
            const surfaceScalarField& etaf
        );


    //- Destructor
    ~blended()
    {}


    // Member Functions
        
        //- Return the interfacial species transfer term
        virtual tmp<fvScalarMatrix> interfaceTerm();

        //- Return the mean diffusion coefficient
        virtual tmp<surfaceScalarField> Dmean();
        
        //- Return the interfacial flux field for local Sherwood number calculations
        virtual tmp<surfaceScalarField> interfaceFlux();
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace cstModels
} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //

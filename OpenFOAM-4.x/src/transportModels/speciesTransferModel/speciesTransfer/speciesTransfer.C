/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2011 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "speciesTransfer.H"
#include "addToRunTimeSelectionTable.H"
#include "surfaceFields.H"
#include "fvCFD.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{
// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::speciesTransfer::speciesTransfer
(
    const volScalarField& alpha,
    const surfaceScalarField& etaf
)
:
    speciesProperties_
    (
        IOobject
        (
            "speciesProperties",
            alpha.mesh().time().constant(),
            alpha.mesh(),
            IOobject::MUST_READ,
            IOobject::NO_WRITE
        )
    ),
    species_(),
    alpha_(alpha),
    etaf_(etaf),
    pHe_(),
    pD1_(),
    pD2_()
{
    PtrList<entry> speciesEntries(speciesProperties_.lookup("species"));
    species_.setSize(speciesEntries.size());
    pHe_.setSize(speciesEntries.size());
    pD1_.setSize(speciesEntries.size());
    pD2_.setSize(speciesEntries.size());

    forAll (species_, speciesI)
    {
        species_.set
        (
            speciesI,
            cstModel::New
            (
                speciesEntries[speciesI].keyword(),
                speciesEntries[speciesI].dict(),
                alpha,
                etaf
            )
        );
        pHe_.set
        (
            speciesI,
            new dimensionedScalar
            (
                species_[speciesI].He()
            )
        );
        pD1_.set
        (
            speciesI,
            new dimensionedScalar
            (
                "D1",
                species_[speciesI].D1()
            )
        );
        pD2_.set
        (
            speciesI,
            new dimensionedScalar
            (
                "D2",
                species_[speciesI].D2()
            )
        );
    }
}

// * * * * * * * * * * * * * * * * Member functions  * * * * * * * * * * * * //

Foam::tmp<Foam::fvScalarMatrix> Foam::speciesTransfer::interfacialTerms
(
    const word& name
)
{
    forAll(species_, ci)
    {
        if (species_[ci].name() == name)
        {
            return species_[ci].interfaceTerm();
        }
    }

    FatalErrorIn("speciesTransfer::interfaceTerm(const word& name)")
        << "Species " << name << " not found in dictionary " << speciesProperties_.name()
        << ". Please add this entry or remove species " << name << " from time directory." << abort(FatalError);
}


Foam::tmp<Foam::surfaceScalarField> Foam::speciesTransfer::Dmean
(
    const word& name
)
{
    forAll(species_, ci)
    {
        if (species_[ci].name() == name)
        {
            return species_[ci].Dmean();
        }
    }

    FatalErrorIn("speciesTransfer::Dmean(const word& name)")
        << "Species " << name << " not found in dictionary " << speciesProperties_.name()
        << ". Please add this entry or remove species " << name << " from time directory." << abort(FatalError);
}


Foam::tmp<Foam::surfaceScalarField> Foam::speciesTransfer::interfacialFlux
(
    const word& name
)
{
    forAll(species_, ci)
    {
        if (species_[ci].name() == name)
        {
            //- Compute the interfacial flux field for local Sherwood number calculations
            //return species_[ci].interfaceFlux();
            //- Return the interfacial flux field directly from assembled matrix
            return species_[ci].interfaceTerm().ref().flux();
        }
    }

    FatalErrorIn("speciesTransfer::interfaceFlux(const word& name)")
        << "Species " << name << " not found in dictionary " << speciesProperties_.name()
        << ". Please add this entry or remove species " << name << " from time directory." << abort(FatalError);
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// ************************************************************************* //


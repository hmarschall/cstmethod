/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Version:  2.4.x                               
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

Class
    inverseDistanceAlphaInterpolation

SourceFiles
    inverseDistanceAlphaInterpolation.C

Authors
    Daniel Rettenmaier <rettenmaier@gsc.tu-darmstadt.de>
    Daniel Deising     <deising@mma.tu-darmstadt.de>
    All rights reserved.

Description
    
    You may refer to this software as :
    //- full bibliographic data to be provided

    This code has been developed by :
        Daniel Rettenmaier < rettenmaier@gsc.tu-darmstadt.de> (main developer).
    
    Method Development and Intellectual Property :
        Daniel Rettenmaier <rettenmaier@gsc.tu-darmstadt.de>
        Daniel Deising     <deising@mma.tu-darmstadt.de>
        Holger Marschall   <marschall@mma.tu-darmstadt.de>
        Dieter Bothe       <bothe@mma.tu-darmstadt.de>
        Cameron Tropea     <ctropea@sla.tu-darmstadt.de>

        Mathematical Modeling and Analysis
        Institute for Fluid Mechanics and Aerodynamics
        Center of Smart Interfaces
        Technische Universitaet Darmstadt
       
    If you use this software for your scientific work or your publications,
    please don't forget to acknowledge explicitly the use of it.

\*---------------------------------------------------------------------------*/


#include "inverseDistanceAlphaInterpolation.H"
#include "addToRunTimeSelectionTable.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //

defineTypeNameAndDebug(inverseDistanceAlphaInterpolation, 0);
addToRunTimeSelectionTable(alphaInterpolation, inverseDistanceAlphaInterpolation, dictionary);

// * * * * * * * * * * * * Private Member Functions  * * * * * * * * * * * * //

// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

inverseDistanceAlphaInterpolation::inverseDistanceAlphaInterpolation
(
    const word& name,
    const volScalarField& alpha,
    const List<bool>& isWallPatch
)
:
	alphaInterpolation(name, alpha,isWallPatch),
    lastUpdateTime_ (-1)
{
    this->makeWeights();
}

// * * * * * * * * * * * * * * * * Destructors * * * * * * * * * * * * * * * //

inverseDistanceAlphaInterpolation::~inverseDistanceAlphaInterpolation()
{}

// * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * * /

void inverseDistanceAlphaInterpolation::makeWeights()
{
    if (lastUpdateTime_ == runTime_.timeIndex())
    {
        return;
    }
    lastUpdateTime_ = runTime_.timeIndex();

    const pointField& points = mesh_.points();
    const labelListList& cellPoints = mesh_.cellPoints();
    const vectorField& cellCentres = mesh_.cellCentres();

    //- Allocate storage for weighting factors
    alphaP_.clear();
    alphaP_.setSize(points.size());

    cellPointWeights_.clear();
    cellPointWeights_.setSize(cellPoints.size());

    boundaryFacePointWeights_.clear();
    boundaryFacePointWeights_.setSize(mesh_.nFaces() - mesh_.nInternalFaces());

    scalarField sumWeights(points.size(), 0.0);

    //- Calculate inverse distances between cell centres and points
    //  and store in weighting factor array
    forAll(cellPoints, iCell)
    {
        const labelList& cp = cellPoints[iCell];

        scalarList& cpw = cellPointWeights_[iCell];

        cpw.setSize(cp.size());

        forAll(cp, i)
        {
            label iPoint = cp[i];
            cpw[i] = 1.0/mag(points[iPoint] - cellCentres[iCell]);
            sumWeights[iPoint] += cpw[i];
        }
    }

    //- Calculate weights for boundary faces

    //- first delete all previously calculated cell weights as interpolation
    //  should be done using the face values only; has to be done in a seperate
    //  loop since points might be shared by multiple patches
    forAll(alpha_.boundaryField(), iPatch)
    {
        if (!alpha_.boundaryField()[iPatch].coupled()) // exclude processors and cyclics
        {
            forAll(alpha_.boundaryField()[iPatch], iFace)
            {
                label iFaceGlobal = iFace + mesh_.boundaryMesh()[iPatch].start();
                const face& facePoints = mesh_.faces()[iFaceGlobal];

                forAll(facePoints, i)
                {
                    label iPoint = facePoints[i];
                    const labelList& pointCells = mesh_.pointCells()[iPoint];

                    forAll(pointCells, n)
                    {
                        label iCell = pointCells[n];
                        const labelList& cp = cellPoints[iCell];
                        scalarList& cpw = cellPointWeights_[iCell];
                        forAll(cp, j)
                        {
                            label jPoint = cp[j];
                            if (jPoint == iPoint)
                            {
                                cpw[j] = 0.0;
                            }
                        }
                    }
                    sumWeights[iPoint] = 0.0;
                }
            }
        }
    }

    //- set new weights for boundary points, but excluding wall faces
    const pointField& faceCentres = mesh_.faceCentres();
    forAll(alpha_.boundaryField(), iPatch)
    {
        if (
                (!alpha_.boundaryField()[iPatch].coupled())
             && (!isWallPatch_[iPatch])
           )
        {
            forAll(alpha_.boundaryField()[iPatch], iFace)
            {
                label iFaceGlobal = iFace + mesh_.boundaryMesh()[iPatch].start();
                label iFaceBoundary = iFaceGlobal - mesh_.nInternalFaces();
                const face& facePoints = mesh_.faces()[iFaceGlobal];

                scalarList& fpw = boundaryFacePointWeights_[iFaceBoundary];
                fpw.setSize(facePoints.size());

                forAll(facePoints, i)
                {
                    label iPoint = facePoints[i];
                    fpw[i] = 1.0/mag(points[iPoint] - faceCentres[iFaceGlobal]);
                    sumWeights[iPoint] += fpw[i];
                }
            }
        }
    }

    //- calculate weights for the wall faces
    //- first delete previously calculated face weights as interpolation
    //  should be done using the wall face values only; has to be done in a seperate
    //  loop since points might be shared by multiple patches
    forAll(alpha_.boundaryField(), iPatch)
    {
        if (isWallPatch_[iPatch])
        {
            forAll(alpha_.boundaryField()[iPatch], iFace)
            {
                label iFaceGlobal = iFace + mesh_.boundaryMesh()[iPatch].start();
                const face& facePoints = mesh_.faces()[iFaceGlobal];

                forAll(facePoints, i)
                {
                    label iPoint = facePoints[i];
                    const labelList& pointFaces = mesh_.pointFaces()[iPoint];
                    forAll(pointFaces, j)                                           //weard warum alles so kompliziert?
                    {
                        label jFace = pointFaces[j];
                        label jFaceBoundary = jFace - mesh_.nInternalFaces();
                        if (jFaceBoundary >= 0)
                        {
                            scalarList& fpw = boundaryFacePointWeights_[jFaceBoundary];
                            if (fpw.size() > 0)
                            {
                                face jFacePoints = mesh_.faces()[jFace];
                                forAll(jFacePoints, k)
                                {
                                    label kPoint = jFacePoints[k];
                                    if (iPoint == kPoint)
                                    {
                                        fpw[k] = 0.0;
                                    }
                                }
                            }
                        }
                    }
                    sumWeights[iPoint] = 0.0;
                }
            }
        }
    }

    //- set new weights for wall points
    forAll(alpha_.boundaryField(), iPatch)
    {
        if (isWallPatch_[iPatch])
        {
            forAll(alpha_.boundaryField()[iPatch], iFace)
            {
                label iFaceGlobal = iFace + mesh_.boundaryMesh()[iPatch].start();
                label iFaceBoundary = iFaceGlobal - mesh_.nInternalFaces();
                const face& facePoints = mesh_.faces()[iFaceGlobal];

                scalarList& fpw = boundaryFacePointWeights_[iFaceBoundary];
                fpw.setSize(facePoints.size());

                forAll(facePoints, i)
                {
                    label iPoint = facePoints[i];
                    fpw[i] = 1.0/mag(points[iPoint] - faceCentres[iFaceGlobal]);
                    sumWeights[iPoint] += fpw[i];
                }
            }
        }
    }

    //- synchronize to account for coupled boundaries
    syncTools::syncPointList(mesh_, sumWeights, plusEqOp<scalar>(),0.0);

    //- normalize weights
    forAll(cellPoints, iCell)
    {
        const labelList& cp = cellPoints[iCell];
        scalarList& cpw = cellPointWeights_[iCell];

        forAll(cp, i)
        {
            label iPoint = cp[i];
            cpw[i] /= (sumWeights[iPoint] + VSMALL);
        }
    }

    forAll(alpha_.boundaryField(), iPatch)
    {
        if (!alpha_.boundaryField()[iPatch].coupled())
        {
            forAll(alpha_.boundaryField()[iPatch], iFace)
            {
                label iFaceGlobal = iFace + mesh_.boundaryMesh()[iPatch].start();
                label iFaceBoundary = iFaceGlobal - mesh_.nInternalFaces();
                const face& facePoints = mesh_.faces()[iFaceGlobal];

                scalarList& fpw = boundaryFacePointWeights_[iFaceBoundary];

                forAll(facePoints, i)
                {
                    label iPoint = facePoints[i];
                    fpw[i] /= (sumWeights[iPoint] + VSMALL);
                }
            }
        }
    }

}

void inverseDistanceAlphaInterpolation::interpolate()
{
    if (mesh_.changing())
    {
        this->makeWeights(); // Calculated only once per time step since mesh
                             // changes once at the most per time step
    }

    scalarField alphaP (mesh_.nPoints(), 0.0);

    //- Interpolate from cells to points
    const labelListList& cellPoints = mesh_.cellPoints();
    forAll(cellPoints, iCell)
    {
        const labelList& cp = cellPoints[iCell];
        const scalarList& cpw = cellPointWeights_[iCell];

        forAll(cp, i)
        {
            label iPoint = cp[i];
            alphaP[iPoint] += alpha_[iCell]*cpw[i];
        }
    }

/*
    //- DR set alpha_ on zeroGradient faces for the interpolation of face centered alpha
    //     values to the facePoints
    volScalarField alphaZG = alpha_;
    forAll(alpha_.boundaryField(), iPatch)
    {

        word patchType = alpha_.boundaryField()[iPatch].type();
        word zeroGradient = "zeroGradient";
        word symmetry = "symmetryPlane";

        if (patchType == zeroGradient || patchType == symmetry)
        {
            forAll(alpha_.boundaryField()[iPatch], iFace)
            {
                label iFaceGlobal = iFace + mesh_.boundaryMesh()[iPatch].start();
                label own = mesh_.faceOwner()[iFaceGlobal];

                alphaZG.boundaryFieldRef()[iPatch][iFace] = alpha_[own];
            }
        }
    }
    //
*/
    //- handle boundary values
    forAll(alpha_.boundaryField(), iPatch)
    {
        if (!alpha_.boundaryField()[iPatch].coupled())
        {
            forAll(alpha_.boundaryField()[iPatch], iFace)
            {
                label iFaceGlobal = iFace + mesh_.boundaryMesh()[iPatch].start();
                label iFaceBoundary = iFaceGlobal - mesh_.nInternalFaces();
                const face& facePoints = mesh_.faces()[iFaceGlobal];

                const scalarList& fpw = boundaryFacePointWeights_[iFaceBoundary];

                forAll(facePoints, i)
                {

                    label iPoint = facePoints[i];
                    alphaP[iPoint] += alpha_.boundaryField()[iPatch][iFace]*fpw[i]; //orig
                    //alphaP[iPoint] += alphaZG.boundaryField()[iPatch][iFace]*fpw[i];  //DR
                }
            }
        }
    }

    //- synchronize to account for coupled boundaries
    syncTools::syncPointList(mesh_, alphaP, plusEqOp<scalar>(),0.0);

    alphaP_ = alphaP;
}

void inverseDistanceAlphaInterpolation::updateAlphaPWall()
{
    if (mesh_.changing())
    {
        this->makeWeights(); // SOLLTE NATÜRLICH NUR EINMAL GEMACHT WERDEN!!!
    }

    scalarField alphaP (mesh_.nPoints(), 0.0); //original, ganzes alphaP feld wird 0 gesetzt!?

/*
//- DR alphaP wird nur auf wall faces zu Null gesetzt //Breaks parallel
    scalarField alphaP = alphaP_;
    forAll(alpha_.boundaryField(), iPatch)
    {
        if (isWallPatch_[iPatch])
        {

            forAll(alpha_.boundaryField()[iPatch], iFace)
            {
                label iFaceGlobal = iFace + mesh_.boundaryMesh()[iPatch].start();
                const face& facePoints = mesh_.faces()[iFaceGlobal];

                forAll(facePoints, i)
                {

                    label iPoint = facePoints[i];
                    alphaP[iPoint] = 0.0;
                }
            }
        }
    }
    syncTools::syncPointList(mesh_, alphaP, plusEqOp<scalar>(),0.0);
//
*/

    forAll(alpha_.boundaryField(), iPatch)
    {
        if (isWallPatch_[iPatch])
        {

            forAll(alpha_.boundaryField()[iPatch], iFace)
            {
                label iFaceGlobal = iFace + mesh_.boundaryMesh()[iPatch].start();
                label iFaceBoundary = iFaceGlobal - mesh_.nInternalFaces();
                const face& facePoints = mesh_.faces()[iFaceGlobal];

                const scalarList& fpw = boundaryFacePointWeights_[iFaceBoundary];

                forAll(facePoints, i)
                {

                    label iPoint = facePoints[i];
                    alphaP[iPoint] += alpha_.boundaryField()[iPatch][iFace]*fpw[i];

                }

            }
        }
    }
    //- synchronize to account for coupled boundaries
    syncTools::syncPointList(mesh_, alphaP, plusEqOp<scalar>(),0.0);

    alphaP_ = alphaP;
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
} // End namespace Foam

// ************************************************************************* //

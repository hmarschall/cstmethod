/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2011-2014 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::distributeField

SourceFiles
    distributeField.C

Authors
    Daniel Rettenmaier   <rettenmaier@gsc.tu-darmstadt.de>
    Daniel Deising       <deising@mma.tu-darmstadt.de>
    Christian Kunkelmann (formerly TTD, TU Darmstadt)
    All rights reserved.

Description
    Distributes a field starting with a configuration marked by a marker set
    to a destination set by averaging field information to neighbor cells.
    Additionally the distance of the current cells to the start set can be
    calculated.

    You may refer to this software as :
    //- full bibliographic data to be provided

    This code has been developed by :
        Daniel Rettenmaier (main developer).
    
    Method Development and Intellectual Property :
        Daniel Rettenmaier <rettenmaier@gsc.tu-darmstadt.de>
        Daniel Deising     <deising@mma.tu-darmstadt.de>
        Holger Marschall   <marschall@mma.tu-darmstadt.de>
        Dieter Bothe       <bothe@mma.tu-darmstadt.de>
        Cameron Tropea     <ctropea@sla.tu-darmstadt.de>

        Mathematical Modeling and Analysis
        Institute for Fluid Mechanics and Aerodynamics
        Center of Smart Interfaces
        Technische Universitaet Darmstadt
       
    If you use this software for your scientific work or your publications,
    please don't forget to acknowledge explicitly the use of it.

\*---------------------------------------------------------------------------*/

#ifndef distributeField_H
#define distributeField_H

#include "IOdictionary.H"
#include "volFields.H"
#include "surfaceFields.H"
#include "syncTools.H"


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

/*---------------------------------------------------------------------------*\
                           Class distributeField Declaration
\*---------------------------------------------------------------------------*/

class distributeField
{

public:



    // Constructors

        //- Construct from volume fraction field gamma and IOdictionary
        distributeField(){};
        
    //- Destructor
    virtual ~distributeField(){}

    // Member Functions

/*      //No definition allowed without body to enforce type cast
        //of templated function in each object of that class

        template <typename T>
        void distributeVolField
        (
            GeometricField<T, fvPatchField, volMesh>& field,                       //field to distribute
            boolList& currField,            //list numbered as field with current distribution status                    
            const boolList& destField,      //list numbered as field with width of the destination field 
            int maxLoops,
            int minLoops,
            bool normalizeField                                   
        ) const;

        //- distributes nHat and calculates the signed distance
        void calculateDistance
        (
            boolList& currField,        //list numbered as field holding distance
            volScalarField& distance,   //information distance to interface
            volVectorField& nHatv,
            const boolList& destField, //minimum field to cover
            scalar maxDistance,
            int maxLoops
        ) const;
*/
       #include "distributeField_impl.H"
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif


// ************************************************************************* //

/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2013 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::curvatureSimple

SourceFiles
    curvatureSimple.C

Authors
    Daniel Rettenmaier <rettenmaier@gsc.tu-darmstadt.de>
    Daniel Deising     <deising@mma.tu-darmstadt.de>
    All rights reserved.

Description
    In combination with the implicit interface representation the curvature
    is calculated the same way as in interFoam

    You may refer to this software as :
    //- full bibliographic data to be provided

    This code has been developed by :
        Daniel Rettenmaier <rettenmaier@gsc.tu-darmstadt.de> (main developer).
    
    Method Development and Intellectual Property :
        Daniel Rettenmaier <rettenmaier@gsc.tu-darmstadt.de>
        Daniel Deising     <deising@mma.tu-darmstadt.de>
        Holger Marschall   <marschall@mma.tu-darmstadt.de>
        Dieter Bothe       <bothe@mma.tu-darmstadt.de>
        Cameron Tropea     <ctropea@sla.tu-darmstadt.de>

        Mathematical Modeling and Analysis
        Institute for Fluid Mechanics and Aerodynamics
        Center of Smart Interfaces
        Technische Universitaet Darmstadt
       
    If you use this software for your scientific work or your publications,
    please don't forget to acknowledge explicitly the use of it.

\*---------------------------------------------------------------------------*/

#ifndef curvatureSimple_H
#define curvatureSimple_H

#include "curvature.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

/*---------------------------------------------------------------------------*\
                         Class curvatureSimple Declaration
\*---------------------------------------------------------------------------*/

class curvatureSimple
:
    public curvature
{

public:

    // Runtime type name
        TypeName("curvatureSimple");


    // Constructors

        //- Construct from components
        curvatureSimple
        (
            const word& name,
            const volScalarField& alpha,
            const volVectorField& nHatv,
            const surfaceVectorField& nHatfv,
            const volScalarField& interfaceDensity,
            const List<bool>& isWallPatch,
            const dictionary& transpProp,
            const volScalarField& isInterface
        );


    //- Destructor
    virtual ~curvatureSimple(){}


    // Member Functions

        //- calculates the curvature
        void calculateK();

};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //

/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2013 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "curvature.H"

// * * * * * * * * * * * * * * * * Selectors * * * * * * * * * * * * * * * * //

Foam::autoPtr<Foam::curvature>Foam::curvature::New
(
    const word& name,
    const volScalarField& alpha,
    const volVectorField& nHatv,
    const surfaceVectorField& nHatfv,
    const volScalarField& interfaceDensity,
    const List<bool>& isWallPatch,
    const dictionary& transpProp,
    const volScalarField& isInterface
)
{
    const word curvatureModel = name;

    Info<<"Selecting curvature model " << curvatureModel << endl;

    dictionaryConstructorTable::iterator curvIter =
        dictionaryConstructorTablePtr_->find(curvatureModel);

    if (curvIter == dictionaryConstructorTablePtr_->end())
    {
        FatalErrorIn
        (
            "curvature::New"
            "("
            "const word&"
            "const volScalarField&"
            "const volVectorField&"
            "const surfaceVectorField&"
            "const volScalarField&"
            "const List<bool>&"
            "const dictionary&"
            ")"
        ) << "Unknown curvature type"
          << curvatureModel << nl << nl
          << "Valid curvature models are : " << endl
          << dictionaryConstructorTablePtr_->sortedToc()
          << exit(FatalError);
    }

    return autoPtr<curvature>
    (
            curvIter()
            (
                name, 
                alpha, 
                nHatv, 
                nHatfv, 
                interfaceDensity, 
                isWallPatch, 
                transpProp,
                isInterface
            )
    );
}

// ************************************************************************* //

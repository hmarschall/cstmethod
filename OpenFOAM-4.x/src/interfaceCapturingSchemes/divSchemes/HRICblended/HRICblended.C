/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

\*---------------------------------------------------------------------------*/

#include "HRICblended.H"
#include "fvc.H"
#include "volFields.H"
#include "surfaceFields.H"
#include "upwind.H"

// * * * * * * * * * * * * * Private Member Functions  * * * * * * * * * * * //

// use HRIC scheme in narrow band around interface
// in far-field switch to upwind
Foam::scalar Foam::HRICblended::weight
(
    const volScalarField& phi,
    const scalar cdWeight,
    const scalar faceFlux,
    const scalar& phiP,
    const scalar& phiN,
    const vector& gradcP,
    const vector& gradcN,
    const scalar Cof,
    const vector d,
    const point cP,
    const point cN,
    const label faceI,
    const scalar blendingFactor
) const
{
    // Calculate upwind value, faceFlux C tilde and do a stabilisation

    scalar phict = 0;
    scalar phiupw = 0;
    scalar costheta = 0;

    if (faceFlux > 0)
    {
        phiupw = phiN - 2*(gradcP & d);

        phiupw = max(min(phiupw, vsfMax_), vsfMin_);

       // limit virtual upwind node
        if (blendingFactor != 0)
        {
            const point& p =  cP - (cN - cP);
            phiupw = limitVirtualUpwind(phi, p, faceI, faceFlux, phiupw);
        }

        if ((phiN - phiupw) > 0)
        {
            phict = (phiP - phiupw)/(phiN - phiupw + SMALL);
        }
        else
        {
            phict = (phiP - phiupw)/(phiN - phiupw - SMALL);
        }
    }
    else
    {
        phiupw = phiP + 2*(gradcN & d);

        phiupw = max(min(phiupw, vsfMax_), vsfMin_);

       // limit virtual upwind node
        if (blendingFactor != 0)
        {
            const point& p =  cN - (cP - cN);
            phiupw = limitVirtualUpwind(phi, p, faceI, faceFlux, phiupw);
        }

        if ((phiP - phiupw) > 0)
        {
            phict = (phiN - phiupw)/(phiP - phiupw + SMALL);
        }
        else
        {
            phict = (phiN - phiupw)/(phiP - phiupw - SMALL);
        }
    }

    // HRIC weights
    costheta = blendingFactor;

    scalar phifbdst = 0;
    scalar phifupwt = 0;
    scalar phifHRIC = 0;
    scalar phiftheta = 0;
    scalar weight;

    // Calculate UDS-Scheme value
    phifupwt = phict;

    // Calculate BDS-Scheme value
    if (phict > 0 && phict <= 0.5)  
    {
        phifbdst = 2*phict;
    }
    else if (phict > 0.5 && phict <= 1)
    {
        phifbdst = 1;
    }
    else
    {
        phifbdst = phict;
    }

    // Calculate the weighting factors for HRIC
    if (Cof <= 0.3)
    {
        phifHRIC = phifbdst * costheta + phifupwt * (1-costheta);
        weight = (phifHRIC - phict)/(1 - phict);
    }
    else if (Cof > 0.3 && Cof < 0.7)
    {
        phiftheta = phifbdst * costheta + phifupwt * (1-costheta);
        phifHRIC = phiftheta + (phiftheta - phifupwt) * (0.7-Cof)/(0.7-0.3);
        weight = (phifHRIC - phict)/(1 - phict);
    }
    else  // use upwind
    {
        phifHRIC = phifupwt;
        weight = 0; 
    }

    if (faceFlux > 0)
    {
        return 1 - weight;
    }
    else
    {
        return weight;
    }
}


void Foam::HRICblended::calculateBlendingFactor() const
{
    // Blending factor for HRIC
    blendingFactor_ = Foam::mag(blendingFactor_);
    blendingFactor_ = min(blendingFactor_, scalar(1.));
    blendingFactor_ = Foam::pow(blendingFactor_,0.5);
    blendingFactor_ = max(min(blendingFactor_, scalar(1.)), scalar(0.));
}


namespace Foam
{
//defineNamedTemplateTypeNameAndDebug(HRICblended, 0);
defineTypeNameAndDebug(HRICblended, 0);

surfaceInterpolationScheme<scalar>::addMeshConstructorToTable<HRICblended>
    addHRICblendedMeshConstructorToTable_;

surfaceInterpolationScheme<scalar>::addMeshFluxConstructorToTable<HRICblended>
    addHRICblendedMeshFluxConstructorToTable_;

limitedSurfaceInterpolationScheme<scalar>::addMeshConstructorToTable<HRICblended>
    addHRICblendedMeshConstructorToLimitedTable_;

limitedSurfaceInterpolationScheme<scalar>::
addMeshFluxConstructorToTable<HRICblended>
    addHRICblendedMeshFluxConstructorToLimitedTable_;
}

// ************************************************************************* //

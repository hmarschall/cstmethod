/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

\*---------------------------------------------------------------------------*/

#include "CICSAMblended.H"
#include "fvc.H"
#include "volFields.H"
#include "surfaceFields.H"
#include "upwind.H"
//#include "addToRunTimeSelectionTable.H"

// * * * * * * * * * * * * * Private Member Functions  * * * * * * * * * * * //

Foam::scalar Foam::CICSAMblended::weight
(
    const volScalarField& phi,
    const scalar cdWeight,
    const scalar faceFlux,
    const scalar& phiP,
    const scalar& phiN,
    const vector& gradcP,
    const vector& gradcN,
    const scalar Cof,
    const vector d,
    const point cP,
    const point cN,
    const label faceI,
    const scalar blendingFactor
) const
{
    // Calculate upwind value, faceFlux C tilde and do a stabilisation

    scalar phict = 0;
    scalar phiupw = 0;
    scalar costheta = 0;

    if (faceFlux > 0)
    {
        phiupw = phiN - 2*(gradcP & d);

        phiupw = max(min(phiupw, vsfMax_), vsfMin_);

        // limit virtual upwind node
        if (blendingFactor != 0)
        {
            const point& p =  cP - (cN - cP);
            //phiupw = limitVirtualUpwind(phi, p, faceI, faceFlux, phiupw);
        }

        if ((phiN - phiupw) > 0)
        {
            phict = (phiP - phiupw)/(phiN - phiupw + SMALL);
        }
        else
        {
            phict = (phiP - phiupw)/(phiN - phiupw - SMALL);
        }
    }
    else
    {
        phiupw = phiP + 2*(gradcN & d);

        phiupw = max(min(phiupw, vsfMax_), vsfMin_);

        // limit virtual upwind node
        if (blendingFactor != 0)
        {
            const point& p =  cN - (cP - cN);
            //phiupw = limitVirtualUpwind(phi, p, faceI, faceFlux, phiupw);
        }

        if ((phiP - phiupw) > 0)
        {
            phict = (phiN - phiupw)/(phiP - phiupw + SMALL);
        }
        else
        {
            phict = (phiN - phiupw)/(phiP - phiupw - SMALL);
        }
    }

    // new calculation of costheta:
    costheta = blendingFactor; 

    // set courant to constant value to enhance boundedness at cost of compression
    scalar CourantD = max(CofMax_,Cof);
    //scalar CourantD = Cof; 

    scalar k1 = (3*CourantD*CourantD - 3*CourantD)/(2*CourantD*CourantD + 6*CourantD - 8);
    scalar k2 = CourantD;
    scalar k3 = (3*CourantD + 5)/(2*CourantD + 6);
    scalar weight;

    // original code CICSAM
    if (phict > 0 && phict <= k1)             // use blended scheme 1
    {
        scalar phifCM = phict/(CourantD + SMALL);
        weight = (phifCM - phict)/(1 - phict);
    }
    else if (phict > k1 && phict <= k2)     // use blended scheme 2
    {
        scalar phifHC = phict/(CourantD + SMALL);
        scalar phifUQ = (8*CourantD*phict + (1 - CourantD)*(6*phict + 3))/8;
        scalar phifCM = costheta*phifHC + (1 - costheta)*phifUQ;
        weight = (phifCM - phict)/(1 - phict);
    }
    else if (phict > k2 && phict < k3)     // use blended scheme 3
    {
        scalar phifUQ = (8*CourantD*phict + (1 - CourantD)*(6*phict + 3))/8;
        scalar phifCM = costheta + (1 - costheta)*phifUQ;
        weight = (phifCM - phict)/(1 - phict);
    }
    else if (phict >= k3 && phict <= 1)     // use downwind
    {
        weight = 1;
    }
    else                                    // use upwind
    {
        weight = 0;
    }

    if (faceFlux > 0)
    {
        return 1 - weight;
    }
    else
    {
        return weight;
    }
}


namespace Foam
{
//defineNamedTemplateTypeNameAndDebug(CICSAMblended, 0);
defineTypeNameAndDebug(CICSAMblended, 0);

surfaceInterpolationScheme<scalar>::addMeshConstructorToTable<CICSAMblended>
    addCICSAMblendedMeshConstructorToTable_;

surfaceInterpolationScheme<scalar>::addMeshFluxConstructorToTable<CICSAMblended>
    addCICSAMblendedMeshFluxConstructorToTable_;

limitedSurfaceInterpolationScheme<scalar>::addMeshConstructorToTable<CICSAMblended>
    addCICSAMblendedMeshConstructorToLimitedTable_;

limitedSurfaceInterpolationScheme<scalar>::
addMeshFluxConstructorToTable<CICSAMblended>
    addCICSAMblendedMeshFluxConstructorToLimitedTable_;
}

// ************************************************************************* //

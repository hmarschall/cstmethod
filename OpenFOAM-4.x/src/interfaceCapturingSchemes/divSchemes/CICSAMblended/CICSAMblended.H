/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

Class
    Foam::CICSAMblended

SourceFiles
    CICSAMblended.C

Authors
    Hrvoje Jasak       Wikki Ltd.
    Daniel Deising     <deising@mma.tu-darmstadt.de>
    All rights reserved.

Description
    CICSAM blended differencing scheme. Uses CICSAM at interface and high
    resolution base scheme elsewhere.

    You may refer to this software as :
    //- Deising et. al. 2016, Chem. Eng. Sci. 139, pp. 173-195

    This code has been developed by :
        Daniel Deising (main developer).
    
    Method Development and Intellectual Property :
        Daniel Deising   <deising@mma.tu-darmstadt.de>
        Holger Marschall <marschall@mma.tu-darmstadt.de>
        Dieter Bothe     <bothe@mma.tu-darmstadt.de>

        Mathematical Modeling and Analysis
        Center of Smart Interfaces
        Technische Universitaet Darmstadt
       
    If you use this software for your scientific work or your publications,
    please don't forget to acknowledge explicitly the use of it.

\*---------------------------------------------------------------------------*/

#ifndef CICSAMblended_H
#define CICSAMblended_H

#include "limitedSurfaceInterpolationScheme.H"
#include "interfaceCapturingScheme.H"
#include "fvc.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

/*---------------------------------------------------------------------------*\
                           Class CICSAMblended Declaration
\*---------------------------------------------------------------------------*/

class CICSAMblended
:
    public interfaceCapturingScheme
{
    // Private data

        // read max courant number from IO to adjust compression
        scalar CofMax_;

        //- all private data already declared in base class 'interfaceCapturingScheme'

    // Private Member Functions

        //- Disallow default bitwise copy construct
        CICSAMblended(const CICSAMblended&);

        //- Disallow default bitwise assignment
        void operator=(const CICSAMblended&);


        //- Calculate individual weight
        scalar weight
        (
            const volScalarField& phi,
            const scalar cdWeight,
            const scalar faceFlux,
            const scalar& phiP,
            const scalar& phiN,
            const vector& gradcP,
            const vector& gradcN,
            const scalar Cof,
            const vector d,
            const point cP,
            const point cN,
            const label faceI,
            const scalar blendingFactor
        ) const;

public:

    //- Runtime type information
    TypeName("CICSAMblended");

    // Constructors

        //- Construct from mesh, faceFlux and blendingFactor
        CICSAMblended
        (
            const fvMesh& mesh,
            const surfaceScalarField& faceFlux//,
            //const volScalarField& vsf
        )
        :
            //interfaceCapturingScheme(mesh, faceFlux, vsf),
            interfaceCapturingScheme(mesh, faceFlux),
            CofMax_(0)
        {}

        //- Construct from mesh and Istream.
        //  The name of the flux field is read from the Istream and looked-up
        //  from the mesh objectRegistry
        CICSAMblended
        (
            const fvMesh& mesh,
            Istream& is
        )
        :
            interfaceCapturingScheme(mesh, is),
            CofMax_(0)
        {
            if (this->schemeData_.found("minCo"))
            {
                 CofMax_ = readScalar(this->schemeData_.lookup("minCo"));
            }
            //if (!is.eof())
            //{
            //    CofMax_ = readScalar(is);
            //}
            //Info<< "max Courant in Scheme: " << CofMax_ << endl;
            //Info<< "vsf.name(): " << vsf_.name() << endl;
        }

        //- Construct from mesh, faceFlux and Istream
        CICSAMblended
        (
            const fvMesh& mesh,
            const surfaceScalarField& faceFlux,
            Istream& is
        )
        :
            interfaceCapturingScheme(mesh, faceFlux, is),
            CofMax_(0)
        {
            if (this->schemeData_.found("minCo"))
            {
                 CofMax_ = readScalar(this->schemeData_.lookup("minCo"));
            }
            //Info<< "max Courant in Scheme: " << CofMax_ << endl;
            //Info<< "vsf.name(): " << vsf_.name() << endl;
        }

};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //

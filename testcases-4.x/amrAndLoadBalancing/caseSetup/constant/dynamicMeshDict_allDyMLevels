/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  2.1.x                                 |
|   \\  /    A nd           | Web:      www.OpenFOAM.org                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "constant";
    object      dynamicMeshDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

dynamicFvMesh   dynamicRefineBalancedFvMesh;

refinementControls
{
    enableRefinementControl  true;
    
    interface
    (
        alpha1
        {
            innerRefLayers 2;
            outerRefLayers 5;
            maxRefineLevel 7;
            interfaceLayers true;
            nAddLayers     4;
        }
    );

    fields
    (
        alpha1 (0.01 1.1 5)
        C0  (0.002 0.05 4)
    );
    
    gradients
    (
        alpha1    (0.01 2 5)
//        C0        (0.001 0.5 3)
    );
    
//    curls
//    (
//        U   (100 1e+05 3)
//    );
    
    regions
    (
        cylinderToCell
        {
            minLevel 1;
            p1       (0.03 0.0   0.03);
            p2       (0.03 0.047 0.03);
            radius   0.015;
        }
        cylinderToCell
        {
            minLevel 2;
            p1       (0.03 0.01  0.03);
            p2       (0.03 0.04 0.03);
            radius   0.009;
        }
        cylinderToCell
        {
            minLevel 3;
            p1       (0.03 0.022 0.03);
            p2       (0.03 0.035 0.03);
            radius   0.005;
        }
    );
}

dynamicRefineFvMeshCoeffs
{
    enableBalancing true;
    allowableImbalance 0.3;
    refineInterval  20;
    field           internalRefinementField;
    lowerRefineLevel 0.5;
    upperRefineLevel 7.5;
    unrefineLevel   -0.5;
    nBufferLayers   1;
    maxRefinement   7;
    maxCells        10000000;
    
    correctFluxes
    (
        (phi none)
        (phiAbs none)
        (phiAbs_0 none)
        (nHatf none)
        (rhoPhi none)
        (ghf none)
      // added for solvers
        (phiCiAdvect none)
        (phiAlphaUpw none)
        (rho*phi none)
        (etaF none)
        (mulesLimiter none)
        (isInterfacef none)
        (etaf none)
        (isInterfaceF none)
        (surfaceTensionForce none)
        (phiAlpha none)
        (phiAlpha_0 none)
        (phi_0 none)
    );
    dumpLevel       true;
}


// ************************************************************************* //
